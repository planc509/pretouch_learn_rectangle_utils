#ifndef RECTANGLE_H
#define RECTANGLE_H
#include <math.h>
class FourPointRectangle;

class TwoPointRectangle{
public:
	TwoPointRectangle(double x1, double y1, double x2, double y2, double theta);
	TwoPointRectangle(double x1, double y1, double x2, double y2, double cos2theta, double sin2theta);
	~TwoPointRectangle();
    double x1() const;
    double y1() const;
    double x2() const;
    double y2() const;
    double theta() const;
    double cos2theta() const;
    double sin2theta() const;
    FourPointRectangle get_four_point_rectangle() const;
    bool point_in_rectangle(double px, double py) const;

private:
    double x1_;
    double y1_;
    double x2_;
    double y2_;
    double theta_;
    double cos2theta_;
    double sin2theta_;
    double cos_theta_;
    double sin_theta_;
	
};

class FourPointRectangle{
public:
    FourPointRectangle(double x1, double y1, double x2, double y2,
                       double x3, double y3, double x4, double y4);
    ~FourPointRectangle();
    double x1() const;
    double y1() const;
    double x2() const;
    double y2() const;
    double x3() const;
    double y3() const;
    double x4() const;
    double y4() const;
    TwoPointRectangle get_two_point_rectangle() const;

private:
    double x1_;
    double y1_;
    double x2_;
    double y2_;
    double x3_;
    double y3_;
    double x4_;
    double y4_;
};

#endif
