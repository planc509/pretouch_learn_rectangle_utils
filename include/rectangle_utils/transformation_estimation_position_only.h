#ifndef TRANSFORMATION_ESTIMATION_POSITION_ONLY_H
#define TRASNFORMATION_ESTIMATION_POSITION_ONLY_H

#include <pcl/registration/transformation_estimation.h>
#include <pcl/cloud_iterator.h>


/** @b TransformationEstimationPositionOnly implements estimation of
  * the transformation aligning the given correspondences when only using translation.
  *
  * \note The class is templated on the source and target point types as well as on the output scalar of the transformation matrix (i.e., float or double). Default: float.
  * \author Dirk Holz, Radu B. Rusu
  * \ingroup registration
  */
template <typename PointSource, typename PointTarget, typename Scalar = float>
class TransformationEstimationPositionOnly : public pcl::registration::TransformationEstimation<PointSource, PointTarget, Scalar>
{
  public:
    typedef boost::shared_ptr<TransformationEstimationPositionOnly<PointSource, PointTarget, Scalar> > Ptr;
    typedef boost::shared_ptr<const TransformationEstimationPositionOnly<PointSource, PointTarget, Scalar> > ConstPtr;

    typedef typename pcl::registration::TransformationEstimation<PointSource, PointTarget, Scalar>::Matrix4 Matrix4;

    /** \brief Constructor
      * \param[in] use_umeyama Toggles whether or not to use 3rd party software*/
    TransformationEstimationPositionOnly (bool use_umeyama=true):
      use_umeyama_ (use_umeyama)
    {}

    virtual ~TransformationEstimationPositionOnly () {};

    /** \brief Estimate a rigid rotation transformation between a source and a target point cloud.
      * \param[in] cloud_src the source point cloud dataset
      * \param[in] cloud_tgt the target point cloud dataset
      * \param[out] transformation_matrix the resultant transformation matrix
      */
    inline void
    estimateRigidTransformation (
        const pcl::PointCloud<PointSource> &cloud_src,
        const pcl::PointCloud<PointTarget> &cloud_tgt,
        Matrix4 &transformation_matrix) const;

    /** \brief Estimate a rigid rotation transformation between a source and a target point cloud.
      * \param[in] cloud_src the source point cloud dataset
      * \param[in] indices_src the vector of indices describing the points of interest in \a cloud_src
      * \param[in] cloud_tgt the target point cloud dataset
      * \param[out] transformation_matrix the resultant transformation matrix
      */
    inline void
    estimateRigidTransformation (
        const pcl::PointCloud<PointSource> &cloud_src,
        const std::vector<int> &indices_src,
        const pcl::PointCloud<PointTarget> &cloud_tgt,
        Matrix4 &transformation_matrix) const;

    /** \brief Estimate a rigid rotation transformation between a source and a target point cloud.
      * \param[in] cloud_src the source point cloud dataset
      * \param[in] indices_src the vector of indices describing the points of interest in \a cloud_src
      * \param[in] cloud_tgt the target point cloud dataset
      * \param[in] indices_tgt the vector of indices describing the correspondences of the interst points from \a indices_src
      * \param[out] transformation_matrix the resultant transformation matrix
      */
    inline void
    estimateRigidTransformation (
        const pcl::PointCloud<PointSource> &cloud_src,
        const std::vector<int> &indices_src,
        const pcl::PointCloud<PointTarget> &cloud_tgt,
        const std::vector<int> &indices_tgt,
        Matrix4 &transformation_matrix) const;

    /** \brief Estimate a rigid rotation transformation between a source and a target point cloud.
      * \param[in] cloud_src the source point cloud dataset
      * \param[in] cloud_tgt the target point cloud dataset
      * \param[in] correspondences the vector of correspondences between source and target point cloud
      * \param[out] transformation_matrix the resultant transformation matrix
      */
    void
    estimateRigidTransformation (
        const pcl::PointCloud<PointSource> &cloud_src,
        const pcl::PointCloud<PointTarget> &cloud_tgt,
        const pcl::Correspondences &correspondences,
        Matrix4 &transformation_matrix) const;

  protected:

    /** \brief Estimate a rigid rotation transformation between a source and a target
      * \param[in] source_it an iterator over the source point cloud dataset
      * \param[in] target_it an iterator over the target point cloud dataset
      * \param[out] transformation_matrix the resultant transformation matrix
      */
    void
    estimateRigidTransformation (pcl::ConstCloudIterator<PointSource>& source_it,
                                 pcl::ConstCloudIterator<PointTarget>& target_it,
                                 Matrix4 &transformation_matrix) const;

    bool use_umeyama_;
 };

#include "rectangle_utils/transformation_estimation_position_only.hpp"

#endif
