#ifndef EXECUTE_RECTANGLE_TRAJ_H
#define EXECUTE_RECTANGLE_TRAJ_H

#include <ros/ros.h>
#include <sstream>
#include <tf/transform_listener.h>
#include "rectangle_utils/ExecuteRectangleTrajSrv.h"
#include "rectangle_utils/SimulateRectangleTrajSrv.h"
#include "pr2_move/GripperMove.h"
#include "pr2_move/ArmMovePoses.h"
#include <tf_conversions/tf_eigen.h>
#include <tf/transform_datatypes.h>
#include <sensor_msgs/JointState.h>
#include <geometry_msgs/PoseArray.h>
#include <moveit/kinematic_constraints/utils.h>
#include <rectangle_utils/icp_fitter.h>

#include <boost/random.hpp>
#include <boost/random/normal_distribution.hpp>
#include <time.h>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl_ros/point_cloud.h>
#include <pcl_ros/transforms.h>

#include "ethercat_hardware/OpticalSensorSample.h"
#include <stdio.h>
#include <termios.h>
#include <unistd.h>

#include <boost/thread/thread.hpp>

#define LEFT_PRETOUCH_CALIBRATION 0.007011
#define RIGHT_PRETOUCH_CALIBRATION 0.00952//0.01125
#define EPS 10e-8

int getch();

class RectangleTrajExecutor {

public:
    RectangleTrajExecutor(ros::NodeHandle& node,
                          std::string& traj_service_topic,
                          std::string& sim_traj_service_topic);
    ~RectangleTrajExecutor();

private:
    typedef pcl::PointXYZ Point;
    typedef pcl::PointCloud<pcl::PointXYZ> PointCloud;

    void apply_transform(const geometry_msgs::PoseStamped& msg_in,
                         const tf::StampedTransform& transform,
                         geometry_msgs::PoseStamped& msg_out);
    bool compute_pretouch_trajectory(const PointCloud::Ptr& unscanned_points,
                                     double pretouch_dist,
                                     double density,
                                     std::vector<geometry_msgs::PoseStamped>& pretouch_traj);

    bool get_cloud_limits(const PointCloud::Ptr& cloud,
                          double& min_x,
                          double& max_x,
                          double& min_y,
                          double& max_y,
                          double& min_z,
                          double& max_z);
    bool rec_traj_to_cam_traj(std::string &side,
                              const std::vector<geometry_msgs::PoseStamped>& rec_poses,
                              const tf::StampedTransform& pretouch_to_wrist,
                              const tf::StampedTransform& rec_to_cam,
                              double min_height,
                              std::vector<geometry_msgs::PoseStamped>& pretouch_traj);
    bool execute_pretouch_traj(const std::string &side,
                               bool separate_begin_traj,
                               const std::vector<geometry_msgs::PoseStamped>& pretouch_traj);
    bool process_pretouch_scan(std::string &side,
                               std::vector<geometry_msgs::PointStamped> &raw_scan,
                               PointCloud::Ptr& scan_in_cam,
                               tf::StampedTransform rec_to_cam,
                               PointCloud::Ptr& scan_in_rec);
    bool update_unscanned_cloud(const PointCloud::Ptr& pretouch_scan_in_rec,
                                double density,
                                const PointCloud::Ptr& unscanned_points_in_rec,
                                PointCloud::Ptr& updated_unscanned_points_in_rec);
    bool rectangle_traj_callback(rectangle_utils::ExecuteRectangleTrajSrv::Request& req,
                                 rectangle_utils::ExecuteRectangleTrajSrv::Response& res);
    bool sim_traj_callback(rectangle_utils::SimulateRectangleTrajSrv::Request& req,
                            rectangle_utils::SimulateRectangleTrajSrv::Response& res);
    void left_pretouch_callback(const ethercat_hardware::OpticalSensorSampleConstPtr& msg);
    void right_pretouch_callback(const ethercat_hardware::OpticalSensorSampleConstPtr& msg);
    void vis_pretouch_traj(const std::vector<geometry_msgs::PoseStamped>& pretouch_traj,
                           bool wait);
    void vis_auto_thread();

    ros::NodeHandle nh_;
    ros::ServiceServer traj_server_;
    ros::ServiceServer sim_traj_server_;
    tf::TransformListener listener_;
    ros::ServiceClient gripper_client_;
    ros::ServiceClient pose_traj_client_;
    std::vector<geometry_msgs::PoseStamped> cur_pretouch_traj_;

    std::vector<geometry_msgs::PointStamped> cur_pretouch_scan_;
    PointCloud::Ptr all_pretouch_scan_;
    long int cur_seq_;
    ros::AsyncSpinner spinner_;
    ros::Subscriber left_pretouch_sub_;
    bool update_left_pretouch_scan_;
    ros::Subscriber right_pretouch_sub_;
    bool update_right_pretouch_scan_;

    ros::Publisher traj_pub_;
    boost::thread pub_thread_;
 };

#endif
