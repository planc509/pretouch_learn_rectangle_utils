#ifndef PROJECT_RECTANGLE
#define PROJECT_RECTANGLE

#include <ros/ros.h>
#include <string>
#include "rectangle_utils/ProjectRectangleSrv.h"
#include <sensor_msgs/CameraInfo.h>
#include "rectangle.h"
#include <tf/transform_listener.h>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl_ros/point_cloud.h>
#include <pcl_ros/transforms.h>
#include <pcl/filters/passthrough.h>
#include <pcl/features/normal_3d.h>
#include <pcl_conversions/pcl_conversions.h>

#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Dense>

#define NORMALS_SEARCH_RADIUS 0.004
#define EPS 10e-8
#define YAW_ONLY_THRESH 0.03

class RectangleProjector{

public:
    RectangleProjector(ros::NodeHandle& node, std::string& service_topic);
    ~RectangleProjector();

private:
    typedef pcl::PointXYZ Point;
    typedef pcl::PointCloud<pcl::PointXYZ> PointCloud;
    typedef pcl::PointCloud<pcl::Normal> PointCloudNormals;

    bool get_rectangle_pcs(const PointCloud::Ptr& cloud_in,
                           const sensor_msgs::CameraInfo& camera_info,
                           const std::vector<TwoPointRectangle> rectangles,
                           std::vector<PointCloud::Ptr>& rectangle_pcs);
    bool filter_table(const PointCloud::Ptr& cloud_in,
                      const PointCloud::Ptr& cloud_out,
                      double table_height);
    bool get_normals(std::string& camera_frame_id,
                     const PointCloud::Ptr& pc,
                     PointCloudNormals& normals);
    bool get_object_origin_and_normal(std::string& camera_frame_id,
                                      const PointCloud::Ptr& box_pc,
                                      geometry_msgs::PointStamped &origin,
                                      geometry_msgs::PointStamped &normal);
    bool rectangle_base_pc_to_transform(const TwoPointRectangle& rectangle,
                                        const PointCloud::Ptr& no_table_base_rectangle_pc,
                                        std::string& camera_link,
                                        tf::Transform& transform,
                                        double table_height);
    bool quaternion_valid(const geometry_msgs::Quaternion& q);
    bool rectangle_service_callback(rectangle_utils::ProjectRectangleSrv::Request& req,
                                    rectangle_utils::ProjectRectangleSrv::Response& res);

    ros::ServiceServer server_;
    tf::TransformListener listener_;
    ros::Time tf_time_;
};

#endif
