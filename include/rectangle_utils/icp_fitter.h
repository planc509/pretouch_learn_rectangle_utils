#ifndef ICP_FITTER_H
#define ICP_FITTER_H

#include <ros/ros.h>
#include <algorithm>
//#include "sparse_icp/ICP.h"

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl_ros/point_cloud.h>
#include <pcl_ros/transforms.h>
#include <pcl/registration/icp.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/registration/icp_nl.h>
#include <pcl/point_representation.h>
#include <pcl/features/normal_3d.h>
#include <pcl/filters/covariance_sampling.h>
#include <pcl/common/common.h>
#include <pcl/registration/correspondence_rejection_sample_consensus.h>
#include <pcl/registration/correspondence_rejection_var_trimmed.h>

#include "rectangle_utils/transformation_estimation_position_only.h"
#include "gaussian_process/SingleGP.h"
#include "gaussian_process/covarianceFunctions.h"

#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Dense>
#include <tf/transform_datatypes.h>
#include <tf_conversions/tf_eigen.h>

class ICPFitter{

public:
    typedef pcl::PointXYZ Point;
    typedef pcl::PointXYZRGB PointRGB;
    typedef pcl::PointCloud<pcl::PointXYZ> PointCloud;
    typedef pcl::PointCloud<pcl::PointXYZRGB> PointCloudRGB;
    typedef pcl::Registration<PointCloud,PointCloud, float>::Matrix4 Matrix4;
    typedef pcl::PointNormal PointNormal;
    typedef pcl::PointCloud<pcl::PointNormal> PointCloudWithNormals;

    ICPFitter(double max_inlier_distance=-1,
              double max_correspondence_distance=-1,
              int max_iterations=-1,
              double ransac_iterations=-1,
              double transform_eps_converge_distance=-1,
              double fitness_eps_converge_distance=-1,
              bool use_normals=false,
              bool use_sparse_icp=false,
              bool use_covar_sampling=false);
    ICPFitter(bool use_reciprocal_correspondences,
              double max_inlier_distance=-1,
              double max_correspondence_distance=-1,
              int max_iterations=-1,
              double ransac_iterations=-1,
              double transform_eps_converge_distance=-1,
              double fitness_eps_converge_distance=-1,
              bool use_normals=false,
              bool use_sparse_icp=false,
              bool use_covar_sampling=false);
    ~ICPFitter();
    void print_settings();
    static bool get_cloud_normals(const PointCloud::Ptr& cloud,
                                  PointCloudWithNormals::Ptr& cloud_normals);
    void set_target_cloud(const PointCloud::Ptr& target_cloud);
    bool do_icp(const PointCloud::Ptr& source_cloud,
                const std::vector<Matrix4>& perturbations,
                std::vector<PointCloud::Ptr>& aligned_clouds,
                std::vector<Matrix4>& transformations,
                std::vector<double >& fitness_scores,
                std::vector<bool>& converged);
    bool do_icp(const std::vector<PointCloud::Ptr>& source_clouds,
                std::vector<PointCloud::Ptr>& aligned_clouds,
                std::vector<Matrix4>& transformations,
                std::vector<double>& fitness_scores,
                std::vector<bool>& converged);
    bool do_icp(const PointCloud::Ptr& source_cloud,
                PointCloud::Ptr& aligned_cloud,
                Matrix4& transformation,
                double& fitness_score);
    bool get_use_reciprocal_correspondences();
    double get_max_inlier_distance();
    double get_max_correspondence_distance();
    int get_max_iterations();
    double get_ransac_iterations();
    double get_transform_eps_converge_distance();
    double get_fitness_eps_converge_distance();

    bool covar_sample(const PointCloudWithNormals::Ptr& cloud,
                      int sample_size,
                      PointCloudWithNormals::Ptr& sample_cloud);
    static bool get_condition_number(const PointCloud::Ptr& source_cloud,
                                     const PointCloud::Ptr& target_cloud,
                                     double max_inlier_distance,
                                     double &condition_number);
    static bool get_average_cloud_density(const PointCloud::Ptr& cloud,
                                          double& avg_distance);
    static bool bilinear_interpolate_cloud(const PointCloud::Ptr& cloud,
                                           bool use_x,
                                           bool use_y,
                                           bool use_z,
                                           double grid_size,
                                           float search_radius,
                                           PointCloud::Ptr& interpolated_cloud);
    /*
    static bool bilinear_interpolate_cloud(const PointCloud::Ptr& cloud,
                                           double grid_size,
                                           float search_radius,
                                           PointCloud::Ptr& interpolated_cloud);
                                           */
    static bool gp_interpolate_cloud(const PointCloud::Ptr& cloud,
                                     double grid_size,
                                     PointCloud::Ptr& interpolated_cloud);

private:
    enum COORDINATE_USE{
        USE_XY = 1,
        USE_XZ = 2,
        USE_YZ = 3
    };

    static bool compute_interp_val(const PointCloud::Ptr& cloud,
                                   const std::vector<int>& nn_idx,
                                   const std::vector<double>& distances,
                                   const COORDINATE_USE& cu,
                                   double& interp_val);

    bool use_normals_;
    bool use_sparse_icp_;
    bool use_covar_sampling_;
    pcl::IterativeClosestPoint<Point, Point> icp_point_;
    pcl::registration::TransformationEstimationSVD<Point, Point>::Ptr tesvd_;
    pcl::IterativeClosestPointWithNormals<PointNormal, PointNormal> icp_normal_;
    pcl::registration::TransformationEstimationPointToPlaneLLS<PointNormal, PointNormal>::Ptr tenorm_;
    PointCloud::Ptr sparse_icp_target_cloud_;
    PointCloudWithNormals::Ptr sparse_icp_target_normals_cloud_;
};

#endif
