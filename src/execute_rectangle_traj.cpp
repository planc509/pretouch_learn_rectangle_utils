#include "rectangle_utils/execute_rectangle_traj.h"

RectangleTrajExecutor::RectangleTrajExecutor(ros::NodeHandle& node,
                                             std::string& traj_service_topic,
                                             std::string& sim_traj_service_topic): nh_(node),
                                                                                  traj_server_(nh_.advertiseService(traj_service_topic,
                                                                                                                    &RectangleTrajExecutor::rectangle_traj_callback,
                                                                                                                    this)),
                                                                                  sim_traj_server_(nh_.advertiseService(sim_traj_service_topic,
                                                                                                                         &RectangleTrajExecutor::sim_traj_callback,
                                                                                                                         this)),
                                                                                  listener_(nh_, ros::Duration(3000.0)),
                                                                                  gripper_client_(nh_.serviceClient<pr2_move::GripperMove>("move_gripper", true)),
                                                                                  pose_traj_client_(nh_.serviceClient<pr2_move::ArmMovePoses>("move_arm_poses", true)),
                                                                                  all_pretouch_scan_(new PointCloud()),
                                                                                  cur_seq_(-1),
                                                                                  spinner_(2),
                                                                                  left_pretouch_sub_(nh_.subscribe("/optical/l_gripper_motor",
                                                                                                                   1,
                                                                                                                   &RectangleTrajExecutor::left_pretouch_callback,
                                                                                                                   this)),
                                                                                  update_left_pretouch_scan_(false),
                                                                                  right_pretouch_sub_(nh_.subscribe("/optical/r_gripper_motor",
                                                                                                                    1,
                                                                                                                    &RectangleTrajExecutor::right_pretouch_callback,
                                                                                                                    this)),
                                                                                  update_right_pretouch_scan_(false),
                                                                                  traj_pub_(nh_.advertise<geometry_msgs::PoseArray>("/execute_rectangle_traj/traj",1)),
                                                                                  pub_thread_(boost::bind(&RectangleTrajExecutor::vis_auto_thread, this)){

    srand(static_cast<unsigned>(time(0)));

    ROS_INFO("Waiting for move_gripper service to start...");
    //ros::service::waitForService("move_gripper");
    ROS_INFO("...done waiting");

    ROS_INFO("Waiting for move_arm_poses service to start...");
    //ros::service::waitForService("move_arm_poses");
    ROS_INFO("...done waiting");

    spinner_.start();

    ROS_INFO("Execute rectangle trajectory available on topic: %s", traj_service_topic.c_str());
    ROS_INFO("Simulated trajecotry execution available on topic: %s", sim_traj_service_topic.c_str());
}

RectangleTrajExecutor::~RectangleTrajExecutor() {

}

/*
bool RectangleTrajExecutor::filter_pretouch_scan(const PointCloud::Ptr& raw_cloud,
                                                double begin_y_rec,
                                                double end_y_rec,
                                                const tf::StampedTransform& rec_to_cam,
                                                PointCloud::Ptr& filtered_cloud) {
    double min_y_rec = std::min(begin_y_rec, end_y_rec);
    double max_y_rec = std::max(begin_y_rec, end_y_rec);

    Eigen::Affine3d cam_to_rec;
    tf::transformTFToEigen(rec_to_cam.inverse(), cam_to_rec);
    PointCloud::Ptr raw_in_rec_cloud(new PointCloud());
    pcl::transformPointCloud(*raw_cloud, *raw_in_rec_cloud, cam_to_rec);

    // Divide up into sequences
    std::vector<std::vector<int> > cloud_seq_indices;
    cloud_seq_indices.push_back(std::vector<int>());
    bool in_range = raw_in_rec_cloud->points[0].y >= min_y_rec &&
                    raw_in_rec_cloud->points[0].y <= max_y_rec;
    unsigned int cur_seq = 0;
    for(unsigned int i = 0; i < raw_in_rec_cloud->points.size(); i++) {
        bool pt_in_range = raw_in_rec_cloud->points[i].y >= min_y_rec &&
                           raw_in_rec_cloud->points[i].y <= max_y_rec;
        if(pt_in_range != in_range) { // New sequence
            in_range = pt_in_range;
            cloud_seq_indices.push_back(std::vector<int>());
            cur_seq++;
        }
        cloud_seq_indices[cur_seq].push_back(i);

    }

    // Score each sequence based on adherence to begin_y_rec and end_y_rec
    ROS_INFO("Found %lu sequences", cloud_seq_indices.size());
    int best_seq_index = -1;
    double best_seq_score = std::numeric_limits<float>::max();
    for(unsigned int i = 0; i < cloud_seq_indices.size(); i++) {
        double score = std::pow(begin_y_rec-raw_in_rec_cloud->points[cloud_seq_indices[i][0]].y,2)+
                       std::pow(end_y_rec-raw_in_rec_cloud->points[cloud_seq_indices[i][cloud_seq_indices[i].size()-1]].y, 2);
        if(score < best_seq_score) {
            best_seq_score = score;
            best_seq_index = i;
        }
    }

    assert(best_seq_index >= 0);

    // Put best sequence points in output cloud
    filtered_cloud->header.frame_id = raw_cloud->header.frame_id;
    filtered_cloud->header.stamp = raw_cloud->header.stamp;
    filtered_cloud->is_dense = raw_cloud->is_dense;
    for(unsigned int i = 0; i < cloud_seq_indices[best_seq_index].size(); i++) {
        filtered_cloud->points.push_back(raw_cloud->points[cloud_seq_indices[best_seq_index][i]]);
    }
    filtered_cloud->height = 1;
    filtered_cloud->width = filtered_cloud->points.size();

    return true;
}

bool RectangleTrajExecutor::arm_is_moving(std::string& side) {
    static const double VEL_TOLERANCE = 0.0001;
    sensor_msgs::JointStateConstPtr jointState = ros::topic::waitForMessage<sensor_msgs::JointState>("joint_states");
    bool moving = false;
    int startIdx = 31; // Index for left upper_arm_roll
    if(side.compare("left") != 0) {
        startIdx = 17; // Index for right_upper_arm_roll
    }
    for(int i = startIdx; i < startIdx+7; i++) {
        if(std::abs(jointState->velocity[i]) > VEL_TOLERANCE) {
            moving = true;
        }
    }
    return moving;
}

bool RectangleTrajExecutor::compute_pretouch_trajectory(const std::string& side,
                                                        const tf::StampedTransform& rec_to_cam,
                                                        const PointCloud::Ptr& rectangle_cloud,
                                                        double pretouch_dist,
                                                        double raster_y_margin,
                                                        double raster_y_inc,
                                                        double raster_z_margin,
                                                        std::vector<geometry_msgs::PoseStamped>& pretouch_traj,
                                                        double& begin_y_rec,
                                                        double& end_y_rec) {
    assert(side.compare("right") == 0 || side.compare("left") == 0);
    assert(pretouch_dist > 0.0);

    tf::StampedTransform pretouch_to_wrist;
    std::string pretouch_frame(std::string(1,side[0])+ "_gripper_motor_l_finger_optical_frt");
    std::string wrist_frame(std::string(1,side[0])+"_wrist_roll_link");
    try {
        listener_.lookupTransform(wrist_frame, pretouch_frame, ros::Time(0), pretouch_to_wrist);
    } catch(tf::TransformException& ex) {
        ROS_ERROR("Could not transform from %s to %s: %s", wrist_frame.c_str(),
                                                           pretouch_frame.c_str(),
                                                           ex.what());
        return false;
    }

    // Ignore rotation of pretouch frame, just want offset
    pretouch_to_wrist.setRotation(tf::Quaternion(0.0,0.0,0.0,1.0));

    // Find max and min y
    Eigen::Affine3d cam_to_rec;
    tf::transformTFToEigen(rec_to_cam.inverse(), cam_to_rec);
    PointCloud::Ptr in_rec_cloud(new PointCloud());
    pcl::transformPointCloud(*rectangle_cloud, *in_rec_cloud, cam_to_rec);

    double max_x = -1*std::numeric_limits<float>::max();
    double max_y = -1*std::numeric_limits<float>::max();
    double min_y = std::numeric_limits<float>::max();
    double max_z = -1*std::numeric_limits<float>::max();
    double min_z = std::numeric_limits<float>::max();

    if(in_rec_cloud->points.size() < 10) {
        ROS_ERROR("Rectangle cloud does not have enough points");
        return false;
    }

    for(unsigned int i = 0; i < in_rec_cloud->points.size(); i++) {
        if(in_rec_cloud->points[i].x > max_x) {
            max_x = in_rec_cloud->points[i].x;
        }

        if(in_rec_cloud->points[i].y > max_y) {
            max_y = in_rec_cloud->points[i].y;
        }
        if(in_rec_cloud->points[i].y < min_y) {
            min_y = in_rec_cloud->points[i].y;
        }

        if(in_rec_cloud->points[i].z > max_z) {
            max_z = in_rec_cloud->points[i].z;
        }
        if(in_rec_cloud->points[i].z < min_z) {
            min_z = in_rec_cloud->points[i].z;
        }

    }

    /*
    //std::vector<geometry_msgs::PoseStamped> rec_poses(3);
    //rec_poses[0].pose.position.x = max_x+pretouch_dist;
    //rec_poses[0].pose.position.y = 0.9*min_y;
    //rec_poses[0].pose.position.z = 0.0;
    //rec_poses[0].pose.orientation.x = 0.0;
    //rec_poses[0].pose.orientation.y = 1.0;
    //rec_poses[0].pose.orientation.z = 0.0;
    //rec_poses[0].pose.orientation.w = 0.0;

    //rec_poses[1].pose.position.x = max_x+pretouch_dist;
    //rec_poses[1].pose.position.y = 0.0;
    //rec_poses[1].pose.position.z = 0.0;
    //rec_poses[1].pose.orientation.x = 0.0;
    //rec_poses[1].pose.orientation.y = 1.0;
    //rec_poses[1].pose.orientation.z = 0.0;
    //rec_poses[1].pose.orientation.w = 0.0;

    //rec_poses[2].pose.position.x = max_x+pretouch_dist;
    //rec_poses[2].pose.position.y = 0.9*max_y;
    //rec_poses[2].pose.position.z = 0.0;
    //rec_poses[2].pose.orientation.x = 0.0;
    //rec_poses[2].pose.orientation.y = 1.0;
    //rec_poses[2].pose.orientation.z = 0.0;
    //rec_poses[2].pose.orientation.w = 0.0;


    //if(max_y-min_y < 2*raster_y_margin+(raster_y_min_rows-1)*raster_y_inc ||
    //   max_z-min_z < 2*raster_z_margin+raster_z_min_scan_length) {
    //    ROS_ERROR("Scan area is not large enough");
    //    return false;
    //}

    std::vector<geometry_msgs::PoseStamped> rec_poses;
    double cur_y = min_y+raster_y_margin;
    double cur_z = min_z+raster_z_margin;
    double y_inc = raster_y_inc;
    double z_inc = (max_z-min_z)-2*raster_z_margin;

    while(cur_y < max_y-raster_y_margin) {
        geometry_msgs::PoseStamped this_pose;
        this_pose.pose.position.x = max_x+pretouch_dist;
        this_pose.pose.position.y = cur_y;
        this_pose.pose.position.z = cur_z;
        this_pose.pose.orientation.x = 0.0;
        this_pose.pose.orientation.y = 1.0;
        this_pose.pose.orientation.z = 0.0;
        this_pose.pose.orientation.w = 0.0;
        rec_poses.push_back(this_pose);

        cur_z = cur_z + z_inc;
        z_inc = -1*z_inc;
        geometry_msgs::PoseStamped next_pose;
        next_pose.pose.position.x = max_x+pretouch_dist;
        next_pose.pose.position.y = cur_y;
        next_pose.pose.position.z = cur_z;
        next_pose.pose.orientation.x = 0.0;
        next_pose.pose.orientation.y = 1.0;
        next_pose.pose.orientation.z = 0.0;
        next_pose.pose.orientation.w = 0.0;
        rec_poses.push_back(next_pose);

        cur_y = cur_y + y_inc;
        //ROS_INFO("this_pose: y = %f, z = %f", this_pose.pose.position.y, this_pose.pose.position.z);
        //ROS_INFO("next_pose: y = %f, z = %f", next_pose.pose.position.y, next_pose.pose.position.z);
        //ROS_INFO("");
    }


    std::vector<geometry_msgs::PoseStamped> wrist_rec_poses(rec_poses.size());
    for(unsigned int i = 0; i < wrist_rec_poses.size(); i++) {
        apply_transform(rec_poses[i], pretouch_to_wrist, wrist_rec_poses[i]);
    }

    std::vector<geometry_msgs::PoseStamped> cam_poses(wrist_rec_poses.size());
    for(unsigned int i = 0; i < cam_poses.size(); i++) {
        apply_transform(wrist_rec_poses[i], rec_to_cam, cam_poses[i]);
    }

    pretouch_traj.clear();
    double end_mid_x = (cam_poses[cam_poses.size()-1].pose.position.x+cam_poses[cam_poses.size()-2].pose.position.x)/2.0;
    double start_mid_x = (cam_poses[0].pose.position.x+cam_poses[1].pose.position.x)/2.0;
    if((end_mid_x >= start_mid_x && side.compare("right") == 0) ||
       (end_mid_x <  start_mid_x && side.compare("left")  == 0)) {
        // Adjust quaternion if necessary by rolling 180 degrees.
        // This is in attempt to have a more grasp-friendly trajectory
        tf::Quaternion tf_rot;
        tf::Quaternion roll_180_rot(1.0, 0.0, 0.0, 0.0);

        for(unsigned int i = 0; i < cam_poses.size(); i++) {
            tf::quaternionMsgToTF(cam_poses[i].pose.orientation, tf_rot);
            tf_rot *= roll_180_rot;
            tf::quaternionTFToMsg(tf_rot, cam_poses[i].pose.orientation);
        }
        // Push traj backwards
        for(int i = cam_poses.size()-1; i >= 0; i--) {
            pretouch_traj.push_back(cam_poses[i]);
        }
        begin_y_rec = max_y;
        end_y_rec = min_y;
    } else {

        for(int i = 0; i < cam_poses.size(); i++) {
            pretouch_traj.push_back(cam_poses[i]);
        }
        begin_y_rec = min_y;
        end_y_rec = max_y;
    }


    //min_y_pose_out = min_y_pose_cam;
    //center_y_pose_out = center_y_pose_cam;
    //max_y_pose_out = max_y_pose_cam;
    //min_y_pose_out.header.frame_id = min_y_pose_cam.header.frame_id;
    //min_y_pose_out.header.stamp = min_y_pose_cam.header.stamp;
    //center_y_pose_out.header.frame_id = center_y_pose_cam.header.frame_id;
    //center_y_pose_out.header.stamp = center_y_pose_out.header.stamp;
    //max_y_pose_out.header.frame_id = max_y_pose_cam.header.frame_id;
    //max_y_pose_out.header.stamp = max_y_pose_cam.header.stamp;



    //for(unsigned int i = 0; i < pretouch_traj.size(); i++) {
    //    ROS_INFO("pose %d: x = %f, y = %f, z = %f", i,
    //                                                pretouch_traj[i].pose.position.x,
    //                                                pretouch_traj[i].pose.position.y,
    //                                                pretouch_traj[i].pose.position.z);
    //}

    return true;
}

*/

void RectangleTrajExecutor::apply_transform(const geometry_msgs::PoseStamped &msg_in,
                                            const tf::StampedTransform &transform,
                                            geometry_msgs::PoseStamped &msg_out) {
    tf::assertQuaternionValid(msg_in.pose.orientation);
    tf::Stamped<tf::Pose> p_in, p_out;
    tf::poseStampedMsgToTF(msg_in, p_in);
    p_out.setData(transform*p_in);
    p_out.stamp_ = transform.stamp_;
    p_out.frame_id_ = transform.frame_id_;
    tf::poseStampedTFToMsg(p_out, msg_out);
}
bool RectangleTrajExecutor::compute_pretouch_trajectory(const PointCloud::Ptr& unscanned_points,
                                                        double pretouch_dist,
                                                        double density,
                                                        std::vector<geometry_msgs::PoseStamped>& pretouch_traj) {

    double min_x, max_x;
    double min_y, max_y;
    double min_z, max_z;

    if(!get_cloud_limits(unscanned_points,
                         min_x, max_x,
                         min_y, max_y,
                         min_z, max_z)) {
        ROS_ERROR("Could not get cloud limits");
        return false;
    }

    double cur_y = min_y;
    double cur_z = min_z;
    double y_inc = std::abs(density);
    double z_inc = std::abs(density);
    geometry_msgs::PoseStamped pose;
    pose.header.frame_id = unscanned_points->header.frame_id;
    pcl_conversions::fromPCL(unscanned_points->header.stamp, pose.header.stamp);
    pose.pose.position.x = max_x + pretouch_dist;
    pose.pose.orientation.x = 0.0;
    pose.pose.orientation.y = 1.0;
    pose.pose.orientation.z = 0.0;
    pose.pose.orientation.w = 0.0;

    PointCloud::Ptr yz_cloud(new PointCloud(*unscanned_points));
    for(unsigned int i = 0; i < yz_cloud->points.size(); i++) {
        yz_cloud->points[i].x = 0.0;
    }
    pcl::KdTreeFLANN<Point> unscanned_tree;
    unscanned_tree.setInputCloud(yz_cloud);
    std::vector<int> nn_idx(1);
    std::vector<float> nn_square_dist(1);
    while(cur_y <= max_y) {
        while((z_inc > 0 && cur_z <= max_z) ||
              (z_inc < 0 && cur_z >= min_z)) {
            if(unscanned_tree.nearestKSearch(Point(0.0, cur_y, cur_z), 1, nn_idx, nn_square_dist) > 0) {
                if(std::sqrt(nn_square_dist[0]) < density) {
                    pose.pose.position.y = yz_cloud->points[nn_idx[0]].y;
                    pose.pose.position.z = yz_cloud->points[nn_idx[0]].z;
                    pretouch_traj.push_back(pose);
                }
            }
            cur_z += z_inc;
        }
        z_inc = -1*z_inc;
        cur_y += y_inc;
    }

    return pretouch_traj.size() > 0;
}


bool RectangleTrajExecutor::get_cloud_limits(const PointCloud::Ptr &cloud,
                                             double &min_x, double &max_x,
                                             double &min_y, double &max_y,
                                             double &min_z, double &max_z) {
    if(cloud->points.size() < 1) {
        return false;
    }

    min_x =    std::numeric_limits<float>::max();
    max_x = -1*std::numeric_limits<float>::max();
    min_y =    std::numeric_limits<float>::max();
    max_y = -1*std::numeric_limits<float>::max();
    min_z =    std::numeric_limits<float>::max();
    max_z = -1*std::numeric_limits<float>::max();
    for(unsigned int i = 0; i < cloud->points.size(); i++) {
        if(cloud->points[i].x < min_x) {
            min_x = cloud->points[i].x;
        }
        if(cloud->points[i].x > max_x) {
            max_x = cloud->points[i].x;
        }

        if(cloud->points[i].y < min_y) {
            min_y = cloud->points[i].y;
        }
        if(cloud->points[i].y > max_y) {
            max_y = cloud->points[i].y;
        }

        if(cloud->points[i].z < min_z) {
            min_z = cloud->points[i].z;
        }
        if(cloud->points[i].z > max_z) {
            max_z = cloud->points[i].z;
        }

    }

    return true;
}

bool RectangleTrajExecutor::rec_traj_to_cam_traj(std::string& side,
                                                 const std::vector<geometry_msgs::PoseStamped>& rec_poses,
                                                 const tf::StampedTransform& pretouch_to_wrist,
                                                 const tf::StampedTransform& rec_to_cam,
                                                 double min_height,
                                                 std::vector<geometry_msgs::PoseStamped>& pretouch_traj) {
    assert(rec_poses.size() > 0);
    std::vector<geometry_msgs::PoseStamped> wrist_rec_poses(rec_poses.size());
    for(unsigned int i = 0; i < wrist_rec_poses.size(); i++) {
        apply_transform(rec_poses[i], pretouch_to_wrist, wrist_rec_poses[i]);
    }

    std::vector<geometry_msgs::PoseStamped> cam_poses(wrist_rec_poses.size());
    for(unsigned int i = 0; i < cam_poses.size(); i++) {
        apply_transform(wrist_rec_poses[i], rec_to_cam, cam_poses[i]);
    }

    pretouch_traj.clear();
    double end_mid_x;
    double start_mid_x;
    if(cam_poses.size() > 2) {
        end_mid_x = (cam_poses[cam_poses.size()-1].pose.position.x+cam_poses[cam_poses.size()-2].pose.position.x)/2.0;
        start_mid_x = (cam_poses[0].pose.position.x+cam_poses[1].pose.position.x)/2.0;
    } else {
        end_mid_x = cam_poses[cam_poses.size()-1].pose.position.x;
        start_mid_x = cam_poses[0].pose.position.x;
    }
    if(true/*(end_mid_x >= start_mid_x && side.compare("right") == 0) ||
       (end_mid_x <  start_mid_x && side.compare("left")  == 0)*/) {
        // Adjust quaternion if necessary by rolling 180 degrees.
        // This is in attempt to have a more grasp-friendly trajectory
        tf::Quaternion tf_rot;
        tf::Quaternion roll_180_rot(1.0, 0.0, 0.0, 0.0);

        for(unsigned int i = 0; i < cam_poses.size(); i++) {
            tf::quaternionMsgToTF(cam_poses[i].pose.orientation, tf_rot);
            tf_rot *= roll_180_rot;
            tf::quaternionTFToMsg(tf_rot, cam_poses[i].pose.orientation);
        }
        for(int i = cam_poses.size()-1; i >= 0; i--) {
            pretouch_traj.push_back(cam_poses[i]);
        }

    } else {
        for(int i = 0; i < cam_poses.size(); i++) {
            pretouch_traj.push_back(cam_poses[i]);
        }
    }

    /*
    geometry_msgs::PoseStamped tmp_pose, gripper_pose;
    tmp_pose.header.frame_id = std::string(1,side[0])+"_gripper_motor_l_finger_optical_frt";
    tmp_pose.header.stamp = ros::Time(0);
    tmp_pose.pose.position.x = 0.0;
    tmp_pose.pose.position.y = 0.0;
    tmp_pose.pose.position.z = 0.0;
    tmp_pose.pose.orientation.x = 0.0;
    tmp_pose.pose.orientation.y = 0.0;
    tmp_pose.pose.orientation.z = 0.0;
    tmp_pose.pose.orientation.w = 1.0;

    try {
        listener_.transformPose(rec_to_cam.frame_id_, tmp_pose, gripper_pose);
    } catch(tf::TransformException& ex) {
        ROS_ERROR("Could not transform from %s to %s: %s", tmp_pose.header.frame_id.c_str(), rec_to_cam.frame_id_.c_str(), ex.what());
        return false;
    }

    double begin_dist = std::pow(gripper_pose.pose.position.x-cam_poses[0].pose.position.x,2) +
                        std::pow(gripper_pose.pose.position.y-cam_poses[0].pose.position.y,2) +
                        std::pow(gripper_pose.pose.position.z-cam_poses[0].pose.position.z,2);
    double end_dist = std::pow(gripper_pose.pose.position.x-cam_poses[cam_poses.size()-1].pose.position.x,2) +
                      std::pow(gripper_pose.pose.position.y-cam_poses[cam_poses.size()-1].pose.position.y,2) +
                      std::pow(gripper_pose.pose.position.z-cam_poses[cam_poses.size()-1].pose.position.z,2);
    if(begin_dist < end_dist) {
        for(int i = 0; i < cam_poses.size(); i++) {
            pretouch_traj.push_back(cam_poses[i]);
        }
    } else {
        for(int i = cam_poses.size()-1; i >= 0; i--) {
            pretouch_traj.push_back(cam_poses[i]);
        }
    }*/

    // Make sure each pose is high enough
    if(min_height > 0.0) {
        for(unsigned int i = 0; i < pretouch_traj.size(); i++) {
            geometry_msgs::PoseStamped base_pose;
            try{
               //listener_.waitForTransform("base_link", pretouch_traj[i].header.frame_id, pretouch_traj[i].header.stamp, ros::Duration(3.0));
                listener_.transformPose("base_link", pretouch_traj[i], base_pose);
            } catch(tf::TransformException& ex) {
                ROS_ERROR("Could not transform from %s to base_link: %s", pretouch_traj[i].header.frame_id.c_str(), ex.what());
                return false;
            }
            if(base_pose.pose.position.z < min_height) {
                base_pose.pose.position.z = min_height;
                try{
                    listener_.transformPose(pretouch_traj[i].header.frame_id, base_pose, pretouch_traj[i]);
                } catch(tf::TransformException& ex) {
                    ROS_ERROR("Could not transform from base_link to %s: %s", pretouch_traj[i].header.frame_id.c_str(), ex.what());
                    return false;
                }
            }
        }
    }
    return true;
}

bool RectangleTrajExecutor::execute_pretouch_traj(const std::string& side,
                                                  bool separate_begin_traj,
                                                  const std::vector<geometry_msgs::PoseStamped>& pretouch_traj) {
    cur_seq_ = -1;
    cur_pretouch_scan_.clear();
    cur_pretouch_traj_ = pretouch_traj;
    /*
    ROS_INFO("Press enter to execute this trajectory");
    ros::Rate loop_rate(10);
    while(ros::ok()) {
        int c = getch();
        if(c == '\n') {
            break;
        }
        loop_rate.sleep();
    }
    */
    if(separate_begin_traj) {
        std::vector<geometry_msgs::PoseStamped> begin_traj;
        begin_traj.push_back(pretouch_traj[0]);
        //pretouch_traj.erase(pretouch_traj.begin());
        /* Tell robot to start executing traj (async) */
        pr2_move::ArmMovePoses start_traj_srv;
        start_traj_srv.request.async = false;
        start_traj_srv.request.avoidCollisions = false;
        start_traj_srv.request.poses = begin_traj;
        start_traj_srv.request.side = side;
        start_traj_srv.request.velScale = 1.0;
        start_traj_srv.request.eefStep = 0.01;

        if(!pose_traj_client_.call(start_traj_srv)) {
            ROS_ERROR("Couldn't execute begin trajectory");
            return false;
        }
    }


    /* Start recording pretouch scan */
    if(side.compare("right") == 0) {
        update_right_pretouch_scan_ = true;
    } else {
        update_left_pretouch_scan_ = true;
    }

    pr2_move::ArmMovePoses pretouch_scan_srv;
    pretouch_scan_srv.request.async = false;
    pretouch_scan_srv.request.avoidCollisions = false;
    pretouch_scan_srv.request.poses = pretouch_traj;
    pretouch_scan_srv.request.side = side;
    pretouch_scan_srv.request.velScale = 1.0;
    pretouch_scan_srv.request.eefStep = 0.01;

    if(!pose_traj_client_.call(pretouch_scan_srv)) {
        /* Stop recording */
        if(side.compare("right") == 0) {
            update_right_pretouch_scan_ = false;
        } else {
            update_left_pretouch_scan_ = false;
        }
        ROS_ERROR("Couldn't execute pretouch trajectory");
        return false;
    }

    /* Stop recording */
    if(side.compare("right") == 0) {
        update_right_pretouch_scan_ = false;
    } else {
        update_left_pretouch_scan_ = false;
    }

    return true;
}

bool RectangleTrajExecutor::process_pretouch_scan(std::string& side,
                                                  std::vector<geometry_msgs::PointStamped>& raw_scan,
                                                  PointCloud::Ptr& scan_in_cam,
                                                  tf::StampedTransform rec_to_cam,
                                                  PointCloud::Ptr& scan_in_rec) {
    scan_in_cam->clear();
    scan_in_rec->clear();

    double pretouch_calibration = 0.0;
    if(side.compare("left") == 0) {
        pretouch_calibration = LEFT_PRETOUCH_CALIBRATION;
    } else {
        pretouch_calibration = RIGHT_PRETOUCH_CALIBRATION;
    }
    ROS_INFO("Adding calibration");
    for(unsigned int i = 0; i < raw_scan.size(); i++) {
        raw_scan[i].point.x += pretouch_calibration;
    }

    ROS_INFO("Transforming scan");
    scan_in_cam->points.reserve(raw_scan.size());
    for(unsigned int i = 0; i < raw_scan.size(); i++) {
        geometry_msgs::PointStamped p_out;
        try{
            listener_.waitForTransform(rec_to_cam.frame_id_,
                                       raw_scan[i].header.frame_id,
                                       raw_scan[i].header.stamp,
                                       ros::Duration(3.0));
            listener_.transformPoint(rec_to_cam.frame_id_,
                                     raw_scan[i],
                                     p_out);
        } catch(tf::TransformException& ex) {
            ROS_ERROR("Couldn't transform from %s to %s: %s", raw_scan[i].header.frame_id.c_str(),
                                                              rec_to_cam.frame_id_.c_str(),
                                                              ex.what());
            return false;
        }
        Point p_cloud;
        p_cloud.x = p_out.point.x;
        p_cloud.y = p_out.point.y;
        p_cloud.z = p_out.point.z;
        scan_in_cam->points.push_back(p_cloud);
    }
    pcl_ros::transformPointCloud(*scan_in_cam, *scan_in_rec, rec_to_cam.inverse());
    return true;
}

bool RectangleTrajExecutor::update_unscanned_cloud(const PointCloud::Ptr& pretouch_scan_in_rec,
                                                   double density,
                                                   const PointCloud::Ptr& unscanned_points_in_rec,
                                                   PointCloud::Ptr& updated_unscanned_points_in_rec) {
    density = density*density;
    PointCloud::Ptr yz_scan(new PointCloud(*pretouch_scan_in_rec));
    for(unsigned int i = 0; i < yz_scan->points.size(); i++) {
        yz_scan->points[i].x = 0.0;
    }
    pcl::KdTreeFLANN<Point> scan_tree;
    scan_tree.setInputCloud(yz_scan);

    bool point_removed = false;
    std::vector<int> nn_idx(1);
    std::vector<float> nn_square_dist(1);
    for(int i = 0; i < unscanned_points_in_rec->points.size(); i++) {
        if(scan_tree.nearestKSearch(Point(0.0, unscanned_points_in_rec->points[i].y, unscanned_points_in_rec->points[i].z),
                                    1,
                                    nn_idx,
                                    nn_square_dist) > 0) {
            if(nn_square_dist[0] < density) {
                point_removed = true;
            } else {
                updated_unscanned_points_in_rec->points.push_back(unscanned_points_in_rec->points[i]);
            }


        }
    }
    updated_unscanned_points_in_rec->header.frame_id = unscanned_points_in_rec->header.frame_id;
    updated_unscanned_points_in_rec->header.stamp = unscanned_points_in_rec->header.stamp;
    updated_unscanned_points_in_rec->is_dense = unscanned_points_in_rec->is_dense;
    updated_unscanned_points_in_rec->height = 1;
    updated_unscanned_points_in_rec->width = updated_unscanned_points_in_rec->points.size();

    return point_removed;
}

bool RectangleTrajExecutor::rectangle_traj_callback(rectangle_utils::ExecuteRectangleTrajSrv::Request &req,
                                                    rectangle_utils::ExecuteRectangleTrajSrv::Response &res) {
    if(req.pretouch_dist < 0.0) {
        return false;
    }

    std::string side;
    if(req.arm[0] == 'l') {
        side = "left";
    } else {
        side = "right";
    }

    /* Open the gripper */
    pr2_move::GripperMove open_gripper_srv;
    open_gripper_srv.request.side = side;
    open_gripper_srv.request.effort = -1.0;
    open_gripper_srv.request.position = 0.087;
    open_gripper_srv.request.async = true;
    gripper_client_.call(open_gripper_srv);

    tf::StampedTransform pretouch_to_wrist;
    std::string pretouch_frame(std::string(1,side[0])+ "_gripper_motor_l_finger_optical_frt");
    std::string wrist_frame(std::string(1,side[0])+"_wrist_roll_link");
    try {
        listener_.lookupTransform(wrist_frame, pretouch_frame, ros::Time(0), pretouch_to_wrist);
    } catch(tf::TransformException& ex) {
        ROS_ERROR("Could not transform from %s to %s: %s", wrist_frame.c_str(),
                                                           pretouch_frame.c_str(),
                                                           ex.what());
        return false;
    }
    // Ignore rotation of pretouch frame, just want offset
    pretouch_to_wrist.setRotation(tf::Quaternion(0.0,0.0,0.0,1.0));

    tf::StampedTransform rec_to_cam;
    rec_to_cam.frame_id_ = req.rec_to_head_transform.header.frame_id;
    rec_to_cam.stamp_ = ros::Time(0);
    rec_to_cam.child_frame_id_ = "rec_frame";
    rec_to_cam.setOrigin(tf::Vector3(req.rec_to_head_transform.pose.position.x,
                                     req.rec_to_head_transform.pose.position.y,
                                     req.rec_to_head_transform.pose.position.z));
    rec_to_cam.setRotation(tf::Quaternion(req.rec_to_head_transform.pose.orientation.x,
                                          req.rec_to_head_transform.pose.orientation.y,
                                          req.rec_to_head_transform.pose.orientation.z,
                                          req.rec_to_head_transform.pose.orientation.w));

    PointCloud::Ptr rectangle_cloud(new PointCloud());
    pcl::fromROSMsg(req.rectangle_cloud, *rectangle_cloud);
    pcl_conversions::toPCL(ros::Time(0), rectangle_cloud->header.stamp);

    PointCloud::Ptr unscanned_points_in_rec(new PointCloud());
    pcl_ros::transformPointCloud(*rectangle_cloud, *unscanned_points_in_rec, rec_to_cam.inverse());

    double min_x, max_x,
           min_y, max_y,
           min_z, max_z;
    if(!get_cloud_limits(unscanned_points_in_rec,
                         min_x, max_x,
                         min_y, max_y,
                         min_z, max_z)) {
        ROS_ERROR("Rectangle cloud too small");
        return false;
    }

    min_y = min_y + req.raster_y_margin;
    max_y = max_y - req.raster_y_margin;
    min_z = min_z + req.raster_z_margin;
    max_z = max_z - req.raster_z_margin;
    unscanned_points_in_rec->height = 1;
    for(int i = unscanned_points_in_rec->points.size()-1; i >= 0; i--) {
        if(unscanned_points_in_rec->points[i].y < min_y ||
           unscanned_points_in_rec->points[i].y > max_y ||
           unscanned_points_in_rec->points[i].z < min_z ||
           unscanned_points_in_rec->points[i].z > max_z) {
            unscanned_points_in_rec->points.erase(unscanned_points_in_rec->points.begin()+i);
        }
    }
    unscanned_points_in_rec->width = unscanned_points_in_rec->points.size();
    int n_unscanned_points = unscanned_points_in_rec->width;
    all_pretouch_scan_->clear();
    int retries = req.allowed_retries;

    while(true) {
         //  create trajectory
         std::vector<geometry_msgs::PoseStamped> pretouch_traj_in_rec;
         if(!compute_pretouch_trajectory(unscanned_points_in_rec,
                                         req.pretouch_dist,
                                         req.required_density,
                                         pretouch_traj_in_rec)) {
             ROS_ERROR("Compute pretouch trajectory returned false");
             assert(false);
         }

        std::vector<geometry_msgs::PoseStamped> pretouch_traj;
        if(!rec_traj_to_cam_traj(side,
                                 pretouch_traj_in_rec,
                                 pretouch_to_wrist,
                                 rec_to_cam,
                                 req.min_height,
                                 pretouch_traj)) {
             ROS_ERROR("Could not convert traj");
             assert(false);
        }

         //  execute trajectory
        if(!execute_pretouch_traj(side,
                                  all_pretouch_scan_->points.size() <= 0,
                                  pretouch_traj)) {
            ROS_WARN("Could not execute trajectory");
            break;
        }

        //  reevaluate number of scanned points
        PointCloud::Ptr scan_in_cam(new PointCloud());
        PointCloud::Ptr scan_in_rec(new PointCloud());
        ROS_INFO("Processing pretouch scan");
        if(!process_pretouch_scan(side,
                                  cur_pretouch_scan_,
                                  scan_in_cam,
                                  rec_to_cam,
                                  scan_in_rec)) {
            ROS_ERROR("Process pretouch scan returned false");
            assert(false);
        }
        *all_pretouch_scan_ += *scan_in_cam;
        ROS_INFO("Finished processing pretouch scan");
        if(req.allowed_retries >= 0) {
            PointCloud::Ptr tmp_cloud(new PointCloud());
            tmp_cloud->points.reserve(unscanned_points_in_rec->points.size());
            if(!update_unscanned_cloud(scan_in_rec,
                                       req.required_density,
                                       unscanned_points_in_rec,
                                       tmp_cloud)) {
                ROS_WARN("No progress made");
                retries  = retries - 1;
                if(retries < 0) {
                    break;
                } else {
                    continue;
                }
            }
            unscanned_points_in_rec = tmp_cloud;
        } else {
            break;
        }

        ROS_INFO("Scanned %lu points out of %d", n_unscanned_points-unscanned_points_in_rec->points.size(), n_unscanned_points);
        ROS_INFO("Percent complete: %f", ((float)(n_unscanned_points-unscanned_points_in_rec->points.size())) / n_unscanned_points);
        if(((float)(n_unscanned_points-unscanned_points_in_rec->points.size())) / n_unscanned_points > req.required_coverage) {
            break;
        }

    }

     // Filter out points outside of rectangle
    /*
    PointCloud::Ptr all_pretouch_scan_in_rec(new PointCloud());
     pcl_ros::transformPointCloud(*all_pretouch_scan_, *all_pretouch_scan_in_rec, rec_to_cam.inverse());
     for(int i = all_pretouch_scan_in_rec->points.size()-1; i >= 0; i--) {
        if(all_pretouch_scan_in_rec->points[i].y < min_y ||
           all_pretouch_scan_in_rec->points[i].y > max_y ||
           all_pretouch_scan_in_rec->points[i].z < min_z ||
           all_pretouch_scan_in_rec->points[i].z > max_z) {
            all_pretouch_scan->points.erase(all_pretouch_scan_->points.begin()+i);
        }
     }

    */
     all_pretouch_scan_->height = 1;
     all_pretouch_scan_->width = all_pretouch_scan_->points.size();
     all_pretouch_scan_->header = rectangle_cloud->header;

     bool success = false;
     if(req.allowed_retries >= 0) {
         success = ((float)(n_unscanned_points-unscanned_points_in_rec->points.size())) / n_unscanned_points > req.required_coverage;
     } else {
         success = true;
     }
     if(success) {
         pcl::toROSMsg(*all_pretouch_scan_,res.pretouch_scan);
     }

     return success;

}

bool RectangleTrajExecutor::sim_traj_callback(rectangle_utils::SimulateRectangleTrajSrv::Request& req,
                                              rectangle_utils::SimulateRectangleTrajSrv::Response& res) {

    static boost::mt19937 rng(time(0));
    boost::normal_distribution<> ndx(0.0, req.sensor_sigma);
    boost::normal_distribution<> ndy(0.0, 0.0*req.required_density);
    boost::normal_distribution<> ndz(0.0, 0.0*req.required_density);
    boost::variate_generator<boost::mt19937&,
                             boost::normal_distribution<> > var_norx(rng, ndx);
    boost::variate_generator<boost::mt19937&,
                             boost::normal_distribution<> > var_nory(rng, ndy);
    boost::variate_generator<boost::mt19937&,
                             boost::normal_distribution<> > var_norz(rng, ndz);

    PointCloud::Ptr rec_cloud(new PointCloud());
    pcl::fromROSMsg(req.rectangle_cloud, *rec_cloud);
    tf::StampedTransform rec_to_cam_tf(tf::Transform(tf::Quaternion(req.rec_to_head_transform.pose.orientation.x,
                                                                    req.rec_to_head_transform.pose.orientation.y,
                                                                    req.rec_to_head_transform.pose.orientation.z,
                                                                    req.rec_to_head_transform.pose.orientation.w),
                                                     tf::Vector3(req.rec_to_head_transform.pose.position.x,
                                                                 req.rec_to_head_transform.pose.position.y,
                                                                 req.rec_to_head_transform.pose.position.z)),
                                       req.rec_to_head_transform.header.stamp,
                                       req.rec_to_head_transform.header.frame_id,
                                       "rectangle_frame");
    PointCloud::Ptr rec_cloud_in_rec(new PointCloud());
    pcl_ros::transformPointCloud(*rec_cloud, *rec_cloud_in_rec, rec_to_cam_tf.inverse());

    if(rec_cloud_in_rec->points.size() < 10) {
        ROS_ERROR("Rectangle cloud does not have enough points");
        return false;
    }
    PointCloud::Ptr pretouch_cloud_in_rec(new PointCloud());
    ICPFitter::bilinear_interpolate_cloud(rec_cloud_in_rec,
                                          false,
                                          true,
                                          true,
                                          std::abs(req.required_density),
                                          req.interp_neighborhood_radius,
                                          pretouch_cloud_in_rec);
    for(unsigned int i = 0; i < pretouch_cloud_in_rec->points.size(); i++) {
        pretouch_cloud_in_rec->points[i].x += req.x_offset+var_norx();
        pretouch_cloud_in_rec->points[i].y += req.y_offset+var_nory();
        pretouch_cloud_in_rec->points[i].z += req.z_offset+var_norz();
    }

    PointCloud::Ptr pretouch_cloud(new PointCloud());
    pcl_ros::transformPointCloud(*pretouch_cloud_in_rec, *pretouch_cloud, rec_to_cam_tf);

    pcl::toROSMsg(*pretouch_cloud, res.pretouch_scan);
    res.pretouch_scan.header.frame_id = req.rectangle_cloud.header.frame_id;
    res.pretouch_scan.header.stamp = req.rectangle_cloud.header.stamp;
    return pretouch_cloud->points.size() > 0;
}

void RectangleTrajExecutor::left_pretouch_callback(const ethercat_hardware::OpticalSensorSampleConstPtr& msg) {
    if(!update_left_pretouch_scan_) {
        return;
    }

    if(cur_seq_ < 0) {
        cur_seq_ = msg->header.seq-1;
    }

    if(cur_seq_ != msg->header.seq-1) {
        //ROS_WARN("Dropped %lu messages", msg->header.seq - cur_seq_ - 1);
    }
    cur_seq_ = msg->header.seq;

    cur_pretouch_scan_.push_back(msg->measurements[3]); // Should be front sensor point stamped

}

void RectangleTrajExecutor::right_pretouch_callback(const ethercat_hardware::OpticalSensorSampleConstPtr& msg) {
    if(!update_right_pretouch_scan_) {
        return;
    }

    if(cur_seq_ < 0) {
        cur_seq_ = msg->header.seq-1;
    }

    if(cur_seq_ != msg->header.seq-1) {
        //ROS_WARN("Dropped %lu messages", msg->header.seq - cur_seq_ - 1);
    }
    cur_seq_ = msg->header.seq;

    cur_pretouch_scan_.push_back(msg->measurements[3]); // Should be front sensor point stamped
}

void RectangleTrajExecutor::vis_pretouch_traj(const std::vector<geometry_msgs::PoseStamped>& pretouch_traj,
                                              bool wait) {
    ros::Publisher publishers[pretouch_traj.size()];
    for(unsigned int i = 0; i < pretouch_traj.size(); i++) {
        std::stringstream ss;
        ss << i;
        publishers[i] = (nh_.advertise<geometry_msgs::PoseStamped>("traj_step"+ss.str(),1));
    }

    if(wait) {
        ROS_INFO("Publishing trajectory on topics traj_step 0 - %lu", pretouch_traj.size()-1);
        ROS_INFO("Press enter to continue...");
    }

    ros::Rate loop_rate(10);
    while(ros::ok()) {
        for(unsigned int i = 0; i < pretouch_traj.size(); i++) {
            publishers[i].publish(pretouch_traj[i]);
        }

        if(wait) {
            int c = getch();
            if(c == '\n') {
                break;
            }
            loop_rate.sleep();
        } else {
            break;
        }

    }
}

void RectangleTrajExecutor::vis_auto_thread() {
    ros::Rate loop_rate(10);

    std::vector<ros::Publisher> traj_publishers;
    ros::Publisher pretouch_pub = nh_.advertise<PointCloud>("/execute_rectangle_traj/pretouch_scan",1);
    while(ros::ok()) {

        std::vector<geometry_msgs::PoseStamped> pretouch_traj = cur_pretouch_traj_;
        if(pretouch_traj.size() > 10) {
            std::vector<geometry_msgs::PoseStamped> new_pretouch_traj;
            for(unsigned int i = 0; i < 5; i++) {
                new_pretouch_traj.push_back(pretouch_traj[i]);
            }
            for(int i = 6; i >= 1; i--) {
                new_pretouch_traj.push_back(pretouch_traj[pretouch_traj.size()-i]);
            }
            pretouch_traj = new_pretouch_traj;
        }

        if(pretouch_traj.size() > 0) {
            if(pretouch_traj.size() != traj_publishers.size()) {
                traj_publishers.clear();
                traj_publishers.resize(pretouch_traj.size());
                for(unsigned int i = 0; i < traj_publishers.size(); i++) {
                    std::stringstream ss;
                    ss << i;
                    traj_publishers[i] = nh_.advertise<geometry_msgs::PoseStamped>("traj_step"+ss.str(),1);
                }
            }
            for(unsigned int i = 0; i < pretouch_traj.size(); i++) {
                pretouch_traj[i].header.stamp = ros::Time(0);
                traj_publishers[i].publish(pretouch_traj[i]);
            }
        }

        if(all_pretouch_scan_ && pretouch_pub.getNumSubscribers() > 0) {
            pretouch_pub.publish(*all_pretouch_scan_);
        }
        loop_rate.sleep();
    }

}

int getch() {
  static struct termios oldt, newt;
  tcgetattr( STDIN_FILENO, &oldt);           // save old settings
  newt = oldt;
  newt.c_lflag &= ~(ICANON);                 // disable buffering
  newt.c_cc[VMIN] = 0; newt.c_cc[VTIME] = 0;
  tcsetattr( STDIN_FILENO, TCSANOW, &newt);  // apply new settings

  int c = getchar();  // read character (non-blocking)

  tcsetattr( STDIN_FILENO, TCSANOW, &oldt);  // restore old settings
  return c;
}

int main(int argc, char** argv) {
    ros::init(argc, argv, "execute_rectangle_traj");
    ros::NodeHandle node;

    std::string traj_service_topic = "execute_rectangle_traj";
    std::string sim_traj_service_topic = "simulate_rectangle_traj";
    traj_service_topic = node.param("/execute_rectangle_traj/traj_service_topic", traj_service_topic);
    sim_traj_service_topic = node.param("/execute_rectangle_traj/sim_traj_service_topic", sim_traj_service_topic);
    RectangleTrajExecutor rte(node,
                              traj_service_topic,
                              sim_traj_service_topic);

    ros::spin();
}
