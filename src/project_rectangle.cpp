#include "rectangle_utils/project_rectangle.h"

RectangleProjector::RectangleProjector(ros::NodeHandle& node,
                                       std::string& service_topic): server_(node.advertiseService(service_topic,
                                                                                                  &RectangleProjector::rectangle_service_callback,
                                                                                                  this)),
                                                                    listener_(ros::Duration(300.0)){

    ROS_INFO("Project rectangle service available on topic: %s", service_topic.c_str());
}

RectangleProjector::~RectangleProjector() {

}

bool RectangleProjector::get_rectangle_pcs(const PointCloud::Ptr& cloud_in,
                                           const sensor_msgs::CameraInfo& camera_info,
                                           const std::vector<TwoPointRectangle> rectangles,
                                           std::vector<PointCloud::Ptr>& rectangle_pcs) {
    std::size_t found = camera_info.header.frame_id.find("optical");
    if(found == std::string::npos) {
        ROS_ERROR_ONCE("The utilized frame_id should contain the substring optical, indicating the x and y axes are aligned with the image");
        return false;
    }

    Eigen::Vector3d camera_link_coords;
    Eigen::Vector3d screen_coords;
    Eigen::Matrix3d link_to_screen;
    link_to_screen << camera_info.K[0], camera_info.K[1], camera_info.K[2],
                      camera_info.K[3], camera_info.K[4], camera_info.K[5],
                      camera_info.K[6], camera_info.K[7], camera_info.K[8];
    Point point;
    for(unsigned int i = 0; i < cloud_in->points.size(); i++) {
        camera_link_coords(0) = cloud_in->points[i].x;
        camera_link_coords(1) = cloud_in->points[i].y;
        camera_link_coords(2) = cloud_in->points[i].z;
        if(isnan(camera_link_coords(0))||
           isnan(camera_link_coords(1))||
           isnan(camera_link_coords(2))) {
            continue;
        }
        screen_coords = link_to_screen*camera_link_coords;
        double x = screen_coords(0)/screen_coords(2);
        double y = screen_coords(1)/screen_coords(2);
        for(unsigned int j = 0; j < rectangles.size(); j++) {
            if(rectangles[j].point_in_rectangle(x,y)) {
                point.x = camera_link_coords(0);
                point.y = camera_link_coords(1);
                point.z = camera_link_coords(2);
                rectangle_pcs[j]->points.push_back(point);
            }
        }
    }

    bool valid_rectangle = false;
    for(unsigned int i = 0; i < rectangle_pcs.size(); i++) {
        rectangle_pcs[i]->header.frame_id = cloud_in->header.frame_id;
        pcl_conversions::toPCL(tf_time_,rectangle_pcs[i]->header.stamp);
        rectangle_pcs[i]->height = 1;
        rectangle_pcs[i]->width = rectangle_pcs[i]->points.size();
        rectangle_pcs[i]->is_dense = cloud_in->is_dense;
        if(rectangle_pcs[i]->points.size() > 0) {
            valid_rectangle = true;
        }
    }

    return valid_rectangle;

}

/* NOTE THAT RETURNED PC IS NOW IN BASE_LINK */
bool RectangleProjector::filter_table(const PointCloud::Ptr& cloud_in,
                                      const PointCloud::Ptr& cloud_out,
                                      double table_height) {
    PointCloud::Ptr base_cloud_in(new PointCloud());
    tf::StampedTransform in_to_base_tf;
    try {

        //listener_.waitForTransform("base_link", cloud_in->header.frame_id, tf_time_, ros::Duration(3.0));
        listener_.lookupTransform("base_link", cloud_in->header.frame_id, tf_time_, in_to_base_tf);
        pcl_ros::transformPointCloud(*cloud_in, *base_cloud_in, in_to_base_tf);
    } catch(tf::TransformException ex) {
        ROS_ERROR("Couldn't transform from %s to base_link: %s", cloud_in->header.frame_id.c_str(), ex.what() );
        return false;
    }


    //pcl_ros::transformPointCloud("base_link", *cloud_in, *base_cloud_in, listener_);

    pcl::PassThrough<Point> pass;
    pass.setInputCloud(base_cloud_in);
    pass.setFilterFieldName("z");
    pass.setFilterLimits(table_height, std::numeric_limits<float>::max());
    pass.filter(*cloud_out);

    cloud_out->header.frame_id = "base_link";
    pcl_conversions::toPCL(tf_time_, cloud_out->header.stamp);

    return cloud_out->points.size() > 0;
}

bool RectangleProjector::get_normals(std::string& camera_frame_id,
                                     const PointCloud::Ptr& pc,
                                     PointCloudNormals& normals) {
    // Get viewpoint
    geometry_msgs::PointStamped p_in, p_out;
    p_in.header.frame_id = camera_frame_id;
    p_in.header.stamp = tf_time_;
    p_in.point.x = 0.0;
    p_in.point.y = 0.0;
    p_in.point.z = 0.0;

    try{
        listener_.waitForTransform(pc->header.frame_id, p_in.header.frame_id, p_in.header.stamp, ros::Duration(3.0));
        listener_.transformPoint(pc->header.frame_id, p_in, p_out);
    } catch(tf::TransformException& ex) {
        ROS_ERROR("Couldn't transform from %s to %s: %s", camera_frame_id.c_str(),
                                                          pc->header.frame_id.c_str(),
                                                          ex.what());
        return false;
    }



    pcl::NormalEstimation<Point, pcl::Normal> ne;
    ne.setInputCloud(pc);
    pcl::search::KdTree<Point>::Ptr tree(new pcl::search::KdTree<Point>());
    ne.setSearchMethod(tree);
    ne.setRadiusSearch(NORMALS_SEARCH_RADIUS);
    ne.setViewPoint(p_out.point.x, p_out.point.y, p_out.point.z);
    ne.compute(normals);

    normals.header.frame_id = pc->header.frame_id;
    pcl_conversions::toPCL(tf_time_, normals.header.stamp);

    return normals.points.size() > 0;
}

bool RectangleProjector::get_object_origin_and_normal(std::string& camera_frame_id,
                                                      const PointCloud::Ptr& box_pc,
                                                      geometry_msgs::PointStamped &origin,
                                                      geometry_msgs::PointStamped &normal) {
    /*PointCloudNormals::Ptr normals(new PointCloudNormals);
    if(!get_normals(camera_frame_id, box_pc, *normals)) {
        return false;
    }

    normal.header.frame_id = box_pc->header.frame_id;
    normal.header.stamp = tf_time_;
    normal.point.x = 0.0;
    normal.point.y = 0.0;
    normal.point.z = 0.0;
    unsigned int normal_count = 0;
    for(int i = 0; i < normals->size(); i++) {
        if(!isnan(normals->points[i].normal[0]) &&
           !isnan(normals->points[i].normal[1]) &&
           !isnan(normals->points[i].normal[2])) {
            normal.point.x += normals->points[i].normal[0];
            normal.point.y += normals->points[i].normal[1];
            normal.point.z += normals->points[i].normal[2];
            normal_count++;
        }
    }
    normal.point.x = normal.point.x / normal_count;
    normal.point.y = normal.point.y / normal_count;
    normal.point.z = normal.point.z / normal_count;
    double normal_mag = sqrt(pow(normal.point.x,2)+pow(normal.point.y,2)+pow(normal.point.z,2));
    normal.point.x = normal.point.x / normal_mag;
    normal.point.y = normal.point.y / normal_mag;
    normal.point.z = normal.point.z / normal_mag;*/

    // Just have a normal be straight up?
    normal.point.x = 0.0;
    normal.point.y = 0.0;
    normal.point.z = 1.0;

    origin.header.frame_id = box_pc->header.frame_id;
    origin.header.stamp = tf_time_;
    origin.point.x = 0.0;
    origin.point.y = 0.0;
    origin.point.z = 0.0;
    for(int i = 0; i < box_pc->points.size(); i++) {
        origin.point.x += box_pc->points[i].x;
        origin.point.y += box_pc->points[i].y;
        origin.point.z += box_pc->points[i].z;
    }
    origin.point.x = origin.point.x / box_pc->points.size();
    origin.point.y = origin.point.y / box_pc->points.size();
    origin.point.z = origin.point.z / box_pc->points.size();

    return true;
}

bool RectangleProjector::rectangle_base_pc_to_transform(const TwoPointRectangle& rectangle,
                                                        const PointCloud::Ptr& no_table_base_rectangle_pc,
                                                        std::string& camera_link,
                                                        tf::Transform& transform,
                                                        double table_height) {
    assert(no_table_base_rectangle_pc->header.frame_id.compare("base_link")==0);

    geometry_msgs::PointStamped x_axis, y_axis, z_axis, origin;
    x_axis.header.frame_id = "base_link";
    y_axis.header.frame_id = "base_link";
    z_axis.header.frame_id = "base_link";
    origin.header.frame_id = "base_link";

    // Calculate normals
    if(!get_object_origin_and_normal(camera_link, no_table_base_rectangle_pc, origin, x_axis)) {
        ROS_ERROR("Couldn't compute the origin and/or normal");
        return false;
    }

    if(origin.point.z < table_height) {
        ROS_ERROR("Computed origin is below table");
        return false;
    }

    // Align the normal with the base_link's z-axis if the origin is close to the table
    if(origin.point.z-table_height < YAW_ONLY_THRESH) {
        x_axis.point.x = 0.0;
        x_axis.point.y = 0.0;
        x_axis.point.z = 1.0;
    }

    // Calculate second axis
    geometry_msgs::PointStamped origin_in_camera;
    origin.header.stamp = tf_time_;
    try{
        listener_.waitForTransform(camera_link, origin.header.frame_id, origin.header.stamp, ros::Duration(3.0));
        listener_.transformPoint(camera_link, origin, origin_in_camera);
    }catch(tf::TransformException& ex) {
        ROS_ERROR("Could not transform from %s to %s: %s", origin.header.frame_id.c_str(),
                                                           camera_link.c_str(),
                                                           ex.what());
        return false;
    }


    geometry_msgs::PointStamped tmp_point1, tmp_point2;
    tmp_point1.header.frame_id = camera_link;
    tmp_point1.header.stamp = tf_time_;
    tmp_point1.point.x = origin_in_camera.point.x + cos(rectangle.theta());
    tmp_point1.point.y = origin_in_camera.point.y + sin(rectangle.theta());
    tmp_point1.point.z = origin_in_camera.point.z;

    try{
        listener_.waitForTransform("base_link", tmp_point1.header.frame_id, tmp_point1.header.stamp, ros::Duration(3.0));
        listener_.transformPoint("base_link", tmp_point1, tmp_point2);
    } catch(tf::TransformException& ex) {
        ROS_ERROR("Could not transform from %s to base_link: %s", tmp_point1.header.frame_id.c_str(),
                                                                  ex.what());
        return false;
    }

    // Project point into plane
    // http://stackoverflow.com/questions/9605556/how-to-project-a-3d-point-to-a-3d-plane
    geometry_msgs::Point diff, proj_point;
    diff.x = tmp_point2.point.x-origin.point.x;
    diff.y = tmp_point2.point.y-origin.point.y;
    diff.z = tmp_point2.point.z-origin.point.z;
    double dist = diff.x*x_axis.point.x+diff.y*x_axis.point.y+diff.z*x_axis.point.z;
    proj_point.x = tmp_point2.point.x-dist*x_axis.point.x;
    proj_point.y = tmp_point2.point.y-dist*x_axis.point.y;
    proj_point.z = tmp_point2.point.z-dist*x_axis.point.z;

    y_axis.point.x = proj_point.x-origin.point.x;
    y_axis.point.y = proj_point.y-origin.point.y;
    y_axis.point.z = proj_point.z-origin.point.z;
    double y_axis_mag = sqrt(pow(y_axis.point.x,2)+pow(y_axis.point.y,2)+pow(y_axis.point.z,2));
    y_axis.point.x = y_axis.point.x/y_axis_mag;
    y_axis.point.y = y_axis.point.y/y_axis_mag;
    y_axis.point.z = y_axis.point.z/y_axis_mag;

    // Calculate third axis through cross-product
    z_axis.point.x = x_axis.point.y*y_axis.point.z-x_axis.point.z*y_axis.point.y;
    z_axis.point.y = -1*(x_axis.point.x*y_axis.point.z-x_axis.point.z*y_axis.point.x);
    z_axis.point.z = x_axis.point.x*y_axis.point.y-x_axis.point.y*y_axis.point.x;
    double z_axis_mag = sqrt(pow(z_axis.point.x,2)+pow(z_axis.point.y,2)+pow(z_axis.point.z,2));
    z_axis.point.x = z_axis.point.x / z_axis_mag;
    z_axis.point.y = z_axis.point.y / z_axis_mag;
    z_axis.point.z = z_axis.point.z / z_axis_mag;

    tf::Matrix3x3 rot_mat(x_axis.point.x, y_axis.point.x, z_axis.point.x,
                          x_axis.point.y, y_axis.point.y, z_axis.point.y,
                          x_axis.point.z, y_axis.point.z, z_axis.point.z);
    tf::Quaternion plane_rot;
    rot_mat.getRotation(plane_rot);

    transform.setOrigin(tf::Vector3(origin.point.x, origin.point.y, origin.point.z));
    transform.setRotation(plane_rot);

    return true;
}

bool RectangleProjector::quaternion_valid(const geometry_msgs::Quaternion& q) {
    double mag = std::sqrt(std::pow(q.x,2)+
                           std::pow(q.y,2)+
                           std::pow(q.z,2)+
                           std::pow(q.w,2));
    return std::abs(mag-1.0) < EPS;
}

bool RectangleProjector::rectangle_service_callback(rectangle_utils::ProjectRectangleSrv::Request& req,
                                                    rectangle_utils::ProjectRectangleSrv::Response& res) {
    tf_time_ = ros::Time(0);//ros::Time::now();
    PointCloud::Ptr object_cloud(new PointCloud());
    pcl::fromROSMsg(req.object_cloud, *object_cloud);
    std::vector<TwoPointRectangle> rectangles;
    std::vector<PointCloud::Ptr> rectangle_pcs;
    for(int i = 0; i < req.rectangles.size(); i++) {
        rectangles.push_back(TwoPointRectangle(req.rectangles[i].x1,
                                               req.rectangles[i].y1,
                                               req.rectangles[i].x2,
                                               req.rectangles[i].y2,
                                               req.rectangles[i].angle));
        rectangle_pcs.push_back(PointCloud::Ptr(new PointCloud()));

    }

    ROS_INFO("Getting rectangle pc...");
    if(!get_rectangle_pcs(object_cloud, req.camera_info, rectangles, rectangle_pcs)) {
        ROS_ERROR("Couldn't get rectangle pc");
        return false;
    }
    ROS_INFO("...got rectangle pc");

    ROS_INFO("Removing table from rectangle pc...");
    // Create pcs with table filtered out
    bool valid_cloud = false;
    std::vector<PointCloud::Ptr> no_table_base_pcs;
    for(unsigned int i = 0; i < rectangle_pcs.size(); i++) {
        no_table_base_pcs.push_back(PointCloud::Ptr(new PointCloud()));
        if(filter_table(rectangle_pcs[i], no_table_base_pcs[i], req.table_height)) {
            valid_cloud = true;
        }
    }
    ROS_INFO("...removed table from rectangle pc");
    if(!valid_cloud) {
        ROS_ERROR("Could not remove table");
        return false;
    }

    ROS_INFO("Computing transform...");
    // Get transforms
    bool valid_transform = false;
    std::vector<tf::Transform> transforms;
    for(unsigned int i = 0; i < no_table_base_pcs.size(); i++) {
        tf::Transform transform;
        if(no_table_base_pcs[i]->points.size() > 0) {
            if(rectangle_base_pc_to_transform(rectangles[i],
                                              no_table_base_pcs[i],
                                              rectangle_pcs[i]->header.frame_id,
                                              transform,
                                              req.table_height)) {
                valid_transform = true;
            }

        }

        transforms.push_back(transform);
    }

    ROS_INFO("... done computing transform");
    if(!valid_transform) {
        ROS_ERROR("Computed invalid transform");
        return false;
    }

    // Load clouds and transforms into results
    res.rectangle_clouds.resize(rectangle_pcs.size());
    res.rectangle_transforms.resize(transforms.size());
    res.valid.resize(transforms.size());
    for(unsigned int i = 0; i < rectangle_pcs.size(); i++) {
        pcl::toROSMsg(*(rectangle_pcs[i]), res.rectangle_clouds[i]);

        geometry_msgs::PoseStamped tmp_pose;
        tmp_pose.header.frame_id = "base_link";
        tmp_pose.header.stamp = tf_time_;
        tmp_pose.pose.position.x = transforms[i].getOrigin().getX();
        tmp_pose.pose.position.y = transforms[i].getOrigin().getY();
        tmp_pose.pose.position.z = transforms[i].getOrigin().getZ();
        tmp_pose.pose.orientation.x = transforms[i].getRotation().getX();
        tmp_pose.pose.orientation.y = transforms[i].getRotation().getY();
        tmp_pose.pose.orientation.z = transforms[i].getRotation().getZ();
        tmp_pose.pose.orientation.w = transforms[i].getRotation().getW();
        try{
            listener_.waitForTransform(req.object_cloud.header.frame_id, tmp_pose.header.frame_id, tmp_pose.header.stamp, ros::Duration(3.0));
            listener_.transformPose(req.object_cloud.header.frame_id, tmp_pose, res.rectangle_transforms[i]);
        } catch(tf::TransformException ex) {
            ROS_ERROR("Could not transform from %s to %s: %s", tmp_pose.header.frame_id.c_str(),
                                                               req.object_cloud.header.frame_id.c_str(),
                                                               ex.what());
            return false;
        }

        res.valid[i] = (rectangle_pcs[i]->points.size() > 0 && quaternion_valid(tmp_pose.pose.orientation));

    }

    return true;

}

int main(int argc, char** argv) {
    ros::init(argc, argv, "project_rectangle_node");
    ros::NodeHandle node;

    std::string service_topic = "project_rectangle";
    service_topic = node.param("/project_rectangle/service_topic", service_topic);

    RectangleProjector rp(node, service_topic);
    ros::spin();
}
