#include "rectangle_utils/rectangle.h"

/*********** Two Point Rectangle Impl **************************************************************************************************************/

TwoPointRectangle::TwoPointRectangle(double x1, double y1,
                                     double x2, double y2,
                                     double theta): x1_(x1), y1_(y1),
                                                    x2_(x2), y2_(y2),
                                                    theta_(theta),
                                                    cos2theta_(cos(2*theta_)),
                                                    sin2theta_(sin(2*theta_)),
                                                    cos_theta_(cos(theta_)),
                                                    sin_theta_(sin(theta_)){

}

TwoPointRectangle::TwoPointRectangle(double x1, double y1,
                                     double x2, double y2,
                                     double cos2theta, double sin2theta): x1_(x1), y1_(y1),
                                                                          x2_(x2), y2_(y2),
                                                                          cos2theta_(cos2theta),
                                                                          sin2theta_(sin2theta),
                                                                          theta_(0.5*atan2(sin2theta_,cos2theta_)),
                                                                          cos_theta_(cos(theta_)),
                                                                          sin_theta_(sin(theta_)){

}

TwoPointRectangle::~TwoPointRectangle() {

}

double TwoPointRectangle::x1() const {
    return x1_;
}

double TwoPointRectangle::y1() const {
    return y1_;
}
double TwoPointRectangle::x2() const {
    return x2_;
}
double TwoPointRectangle::y2() const {
    return y2_;
}

double TwoPointRectangle::theta() const {
    return theta_;
}

double TwoPointRectangle::cos2theta() const {
    return cos2theta_;
}

double TwoPointRectangle::sin2theta() const {
    return sin2theta_;
}

FourPointRectangle TwoPointRectangle::get_four_point_rectangle() const {
    double mid_x = 0.5*(x1_+x2_);
    double mid_y = 0.5*(y1_+y2_);

    double x1 = x1_-mid_x;
    double y1 = y1_-mid_y;

    double x2 = x2_-mid_x;
    double y2 = y1_-mid_y;

    double x3 = x1_-mid_x;
    double y3 = y2_-mid_y;

    double x4 = x2_-mid_x;
    double y4 = y2_-mid_y;

    double cos_val = cos(theta_);
    double sin_val = sin(theta_);

    double x1_prime = x1*cos_val-y1*sin_val;
    double y1_prime = x1*sin_val+y1*cos_val;

    double x2_prime = x2*cos_val-y2*sin_val;
    double y2_prime = x2*sin_val+y2*cos_val;

    double x3_prime = x3*cos_val-y3*sin_val;
    double y3_prime = x3*sin_val+y3*cos_val;

    double x4_prime = x4*cos_val-y4*sin_val;
    double y4_prime = x4*sin_val+y4*cos_val;

    return FourPointRectangle(x1_prime+mid_x, y1_prime+mid_y,
                              x2_prime+mid_x, y2_prime+mid_y,
                              x3_prime+mid_x, y3_prime+mid_y,
                              x4_prime+mid_x, y4_prime+mid_y);

}

bool TwoPointRectangle::point_in_rectangle(double px, double py) const {
    double mid_x = 0.5*(x1_+x2_);
    double mid_y = 0.5*(y1_+y2_);

    // Rotate point
    double cos_val = cos_theta_; // cos(-theta) = cos(theta)
    double sin_val = -1*sin_theta_; // sin(-theta) = -1*sin(theta)
    double p_rot_x = (px-mid_x)*cos_val-(py-mid_y)*sin_val;
    double p_rot_y = (px-mid_x)*sin_val+(py-mid_y)*cos_val;
    p_rot_x += mid_x;
    p_rot_y += mid_y;

    return (x1_ <= p_rot_x &&
            p_rot_x <= x2_ &&
            y1_ <= p_rot_y &&
            p_rot_y <= y2_);
}

/*********** Four Point Rectangle Impl **************************************************************************************************************/
FourPointRectangle::FourPointRectangle(double x1, double y1,
                                       double x2, double y2,
                                       double x3, double y3,
                                       double x4, double y4): x1_(x1), y1_(y1),
                                                              x2_(x2), y2_(y2),
                                                              x3_(x3), y3_(y3),
                                                              x4_(x4), y4_(y4){

}

FourPointRectangle::~FourPointRectangle() {

}

double FourPointRectangle::x1() const {
    return x1_;
}

double FourPointRectangle::y1() const {
    return y1_;
}

double FourPointRectangle::x2() const {
    return x2_;
}

double FourPointRectangle::y2() const {
    return y2_;
}

double FourPointRectangle::x3() const {
    return x3_;
}

double FourPointRectangle::y3() const {
    return y3_;
}

double FourPointRectangle::x4() const {
    return x4_;
}

double FourPointRectangle::y4() const {
    return y4_;
}

TwoPointRectangle FourPointRectangle::get_two_point_rectangle() const {
    double angle = atan2(x2_-x4_, y4_-y2_);
    double mid_x = 0.5*(x1_+x4_);
    double mid_y = 0.5*(y1_+y4_);

    double cos_val = cos(-1*angle);
    double sin_val = sin(-1*angle);

    double x1 = (x1_-mid_x)*cos_val-(y1_-mid_y)*sin_val;
    double y1 = (x1_-mid_x)*sin_val+(y1_-mid_y)*cos_val;

    double x2 = (x4_-mid_x)*cos_val-(y4_-mid_y)*sin_val;
    double y2 = (x4_-mid_x)*sin_val+(y4_-mid_y)*cos_val;

    return TwoPointRectangle(x1+mid_x,y1+mid_y,
                             x2+mid_x,y2+mid_y,
                             angle);

}
