#include "rectangle_utils/icp_fitter.h"

ICPFitter::ICPFitter(bool use_reciprocal_correspondences,
                     double max_inlier_distance,
                     double max_correspondence_distance,
                     int max_iterations,
                     double ransac_iterations,
                     double transform_eps_converge_distance,
                     double fitness_eps_converge_distance,
                     bool use_normals,
                     bool use_sparse_icp,
                     bool use_covar_sampling):
                                                           use_normals_(use_normals),
                                                           use_sparse_icp_(use_sparse_icp),
                                                           use_covar_sampling_(use_covar_sampling),
                                                           //tepo_(new TransformationEstimationPositionOnly<Point,Point>(false)),
                                                           tesvd_(new pcl::registration::TransformationEstimationSVD<Point,Point>(false)),
                                                           tenorm_(new pcl::registration::TransformationEstimationPointToPlaneLLS<PointNormal, PointNormal>()){

    if(max_inlier_distance > 0) {
        //icp_position_only_.setRANSACOutlierRejectionThreshold(max_inlier_distance);
        icp_point_.setRANSACOutlierRejectionThreshold(max_inlier_distance);
        icp_normal_.setRANSACOutlierRejectionThreshold(max_inlier_distance);
    }
    if(max_correspondence_distance > 0) {
        //icp_position_only_.setRANSACOutlierRejectionThreshold(max_inlier_distance);
        icp_point_.setMaxCorrespondenceDistance(max_correspondence_distance);
        icp_normal_.setMaxCorrespondenceDistance(max_correspondence_distance);
    }
    if(max_iterations > 0) {
        //icp_position_only_.setRANSACOutlierRejectionThreshold(max_inlier_distance);
        icp_point_.setMaximumIterations(max_iterations);
        icp_normal_.setMaximumIterations(max_iterations);
    }
    if(ransac_iterations > 0) {
        //icp_position_only_.setRANSACOutlierRejectionThreshold(max_inlier_distance);
        icp_point_.setRANSACIterations(ransac_iterations);
        icp_normal_.setRANSACIterations(ransac_iterations);
    }
    if(transform_eps_converge_distance > 0) {
        //icp_position_only_.setRANSACOutlierRejectionThreshold(max_inlier_distance);
        icp_point_.setTransformationEpsilon(transform_eps_converge_distance);
        icp_normal_.setTransformationEpsilon(transform_eps_converge_distance);
    }
    if(fitness_eps_converge_distance > 0) {
        //icp_position_only_.setRANSACOutlierRejectionThreshold(max_inlier_distance);
        icp_point_.setEuclideanFitnessEpsilon(fitness_eps_converge_distance);
        icp_normal_.setEuclideanFitnessEpsilon(fitness_eps_converge_distance);
    }
    //icp_position_only_.setUseReciprocalCorrespondences(use_reciprocal_correspondences);
    icp_point_.setUseReciprocalCorrespondences(use_reciprocal_correspondences);
    icp_normal_.setUseReciprocalCorrespondences(use_reciprocal_correspondences);

    //icp_position_only_.setTransformationEstimation(tepo_);
    icp_point_.setTransformationEstimation(tesvd_);
    icp_normal_.setTransformationEstimation(tenorm_);

    pcl::registration::CorrespondenceRejectorSampleConsensus<Point>::Ptr crsc(new pcl::registration::CorrespondenceRejectorSampleConsensus<Point>());
    pcl::registration::CorrespondenceRejectorVarTrimmed::Ptr crvt(new pcl::registration::CorrespondenceRejectorVarTrimmed());
    icp_point_.addCorrespondenceRejector(crsc);
    icp_point_.addCorrespondenceRejector(crvt);
}

ICPFitter::ICPFitter(double max_inlier_distance,
                     double max_correspondence_distance,
                     int max_iterations,
                     double ransac_iterations,
                     double transform_eps_converge_distance,
                     double fitness_eps_converge_distance,
                     bool use_normals,
                     bool use_sparse_icp,
                     bool use_covar_sampling):
                                        use_normals_(use_normals),
                                        use_sparse_icp_(use_sparse_icp),
                                        use_covar_sampling_(use_covar_sampling),
                                        //tepo_(new TransformationEstimationPositionOnly<Point,Point>(false)),
                                        tesvd_(new pcl::registration::TransformationEstimationSVD<Point,Point>(false)),
                                        tenorm_(new pcl::registration::TransformationEstimationPointToPlaneLLS<PointNormal, PointNormal>()){
    if(max_inlier_distance > 0) {
        //icp_position_only_.setRANSACOutlierRejectionThreshold(max_inlier_distance);
        icp_point_.setRANSACOutlierRejectionThreshold(max_inlier_distance);
        icp_normal_.setRANSACOutlierRejectionThreshold(max_inlier_distance);
    }
    if(max_correspondence_distance > 0) {
        //icp_position_only_.setRANSACOutlierRejectionThreshold(max_inlier_distance);
        icp_point_.setMaxCorrespondenceDistance(max_correspondence_distance);
        icp_normal_.setMaxCorrespondenceDistance(max_correspondence_distance);
    }
    if(max_iterations > 0) {
        //icp_position_only_.setRANSACOutlierRejectionThreshold(max_inlier_distance);
        icp_point_.setMaximumIterations(max_iterations);
        icp_normal_.setMaximumIterations(max_iterations);
    }
    if(ransac_iterations > 0) {
        //icp_position_only_.setRANSACOutlierRejectionThreshold(max_inlier_distance);
        icp_point_.setRANSACIterations(ransac_iterations);
        icp_normal_.setRANSACIterations(ransac_iterations);
    }
    if(transform_eps_converge_distance > 0) {
        //icp_position_only_.setRANSACOutlierRejectionThreshold(max_inlier_distance);
        icp_point_.setTransformationEpsilon(transform_eps_converge_distance);
        icp_normal_.setTransformationEpsilon(transform_eps_converge_distance);
    }
    if(fitness_eps_converge_distance > 0) {
        //icp_position_only_.setRANSACOutlierRejectionThreshold(max_inlier_distance);
        icp_point_.setEuclideanFitnessEpsilon(fitness_eps_converge_distance);
        icp_normal_.setEuclideanFitnessEpsilon(fitness_eps_converge_distance);
    }

    //icp_position_only_.setTransformationEstimation(tepo_);
    icp_point_.setTransformationEstimation(tesvd_);
    icp_normal_.setTransformationEstimation(tenorm_);

}

ICPFitter::~ICPFitter() {

}

void ICPFitter::print_settings() {
    ROS_INFO("ICP Settings: ");
    if(use_sparse_icp_) {
        ROS_ERROR("Not implemented");
       // ROS_INFO("Using sparse icp");
    }
    if(use_normals_) {
        ROS_INFO("  use_reciprocal_correspondences: %d", icp_normal_.getUseReciprocalCorrespondences());
        ROS_INFO("  max_inlier_distance: %f", icp_normal_.getRANSACOutlierRejectionThreshold());
        ROS_INFO("  max_correspondence_distance: %f", icp_normal_.getMaxCorrespondenceDistance());
        ROS_INFO("  maximum_iterations: %d", icp_normal_.getMaximumIterations());
        ROS_INFO("  ransac_iterations: %f", icp_normal_.getRANSACIterations());
        ROS_INFO("  transform_rps_converge_distance: %f", icp_normal_.getTransformationEpsilon());
        ROS_INFO("  fitness_eps_converge_distance: %f", icp_normal_.getEuclideanFitnessEpsilon());
    } else {
        ROS_INFO("  use_reciprocal_correspondences: %d", icp_point_.getUseReciprocalCorrespondences());
        ROS_INFO("  max_inlier_distance: %f", icp_point_.getRANSACOutlierRejectionThreshold());
        ROS_INFO("  max_correspondence_distance: %f", icp_point_.getMaxCorrespondenceDistance());
        ROS_INFO("  maximum_iterations: %d", icp_point_.getMaximumIterations());
        ROS_INFO("  ransac_iterations: %f", icp_point_.getRANSACIterations());
        ROS_INFO("  transform_rps_converge_distance: %f", icp_point_.getTransformationEpsilon());
        ROS_INFO("  fitness_eps_converge_distance: %f", icp_point_.getEuclideanFitnessEpsilon());
    }

}

bool ICPFitter::covar_sample(const PointCloudWithNormals::Ptr& cloud,
                             int sample_size,
                             PointCloudWithNormals::Ptr& sample_cloud) {
    pcl::CovarianceSampling<PointNormal, PointNormal> cov_sample;
    cov_sample.setNumberOfSamples(sample_size);
    cov_sample.setInputCloud(cloud);
    cov_sample.setNormals(icp_normal_.getInputTarget());
    cov_sample.filter(*sample_cloud);
    return true;
}

bool ICPFitter::get_condition_number(const PointCloud::Ptr& source_cloud,
                                     const PointCloud::Ptr& target_cloud,
                                     double max_inlier_distance,
                                     double& condition_number) {
    max_inlier_distance = (max_inlier_distance*max_inlier_distance);
    pcl::KdTreeFLANN<Point> target_tree;
    target_tree.setInputCloud(target_cloud);

    std::vector<int> nn_idx;
    std::vector<float> nn_square_dist;
    std::vector<int> target_indices;
    std::vector<int> source_indices;
    //ROS_INFO("Finding correspondences...");
    for(unsigned int i = 0; i < source_cloud->points.size(); i++) {
        if(target_tree.nearestKSearch(source_cloud->points[i],
                                      1,
                                      nn_idx,
                                      nn_square_dist)) {
            if(nn_square_dist[0] < max_inlier_distance) {
                target_indices.push_back(nn_idx[0]);
                source_indices.push_back(i);
            }
        }
    }
    if(target_indices.size() <= 0) {
        return false;
    }

    //ROS_INFO("Computing normals...");
    PointCloudWithNormals::Ptr source_normals(new PointCloudWithNormals());
    get_cloud_normals(source_cloud, source_normals);

    //ROS_INFO("Scaling points...");
    Eigen::Vector3f centroid(0.f,0.f,0.f);
    for(size_t p_i = 0; p_i < target_indices.size(); p_i++) {
        centroid += (*target_cloud)[target_indices[p_i]].getVector3fMap();
    }
    centroid /= float(target_indices.size());

    std::vector<Eigen::Vector3f, Eigen::aligned_allocator<Eigen::Vector3f> > scaled_points;
    scaled_points.resize(target_indices.size());
    double average_norm = 0.0;
    for(size_t p_i = 0; p_i < target_indices.size(); p_i++) {
        scaled_points[p_i] = (*target_cloud)[target_indices[p_i]].getVector3fMap()-centroid;
        average_norm += scaled_points[p_i].norm();
    }
    average_norm /= double(scaled_points.size());
    for(size_t p_i = 0; p_i < scaled_points.size(); p_i++) {
        scaled_points[p_i] /= float(average_norm);
    }

    //ROS_INFO("Calculating covariance matrix...");
    Eigen::Matrix<double, 6, Eigen::Dynamic> f_mat = Eigen::Matrix<double, 6, Eigen::Dynamic> (6, target_indices.size());
    for(size_t p_i = 0; p_i < scaled_points.size(); p_i++) {
        f_mat.block<3,1>(0,p_i) = scaled_points[p_i].cross((*source_normals)[source_indices[p_i]].getNormalVector3fMap()).cast<double>();
        f_mat.block<3,1>(3,p_i) = (*source_normals)[source_indices[p_i]].getNormalVector3fMap().cast<double>();
    }

    Eigen::Matrix<double, 6, 6> covariance_matrix = f_mat*f_mat.transpose();

    //ROS_INFO("Computing eigenvalues...");
    Eigen::EigenSolver<Eigen::Matrix<double,6,6> > eigen_solver;
    eigen_solver.compute(covariance_matrix, true);

    Eigen::MatrixXcd complex_eigenvalues = eigen_solver.eigenvalues();
    double max_ev = -std::numeric_limits<double>::max();
    double min_ev = std::numeric_limits<double>::max();
    for(size_t i = 0; i < 6; i++) {
        if(real(complex_eigenvalues(i,0)) > max_ev) {
            max_ev = real(complex_eigenvalues(i,0));
        }
        if(real(complex_eigenvalues(i,0)) < min_ev) {
            min_ev = real(complex_eigenvalues(i,0));
        }
    }

    condition_number = max_ev/min_ev;

    return true;
    /*
    PointCloudWithNormals::Ptr source_cloud_normals(new PointCloudWithNormals());
    get_cloud_normals(source_cloud,
                      source_cloud_normals);
    pcl::copyPointCloud(*source_cloud, *source_cloud_normals);

    PointCloudWithNormals::Ptr target_cloud_normals(new PointCloudWithNormals());
    get_cloud_normals(target_cloud,
                      target_cloud_normals);
    pcl::copyPointCloud(*target_cloud, *target_cloud_normals);

    pcl::CovarianceSampling<PointNormal, PointNormal> cov;
    cov.setNumberOfSamples(source_cloud_normals->points.size()/8);
    cov.setInputCloud(source_cloud_normals);
    cov.setNormals(source_cloud_normals);

    condition_number = cov.computeConditionNumber();

    return true;
    */
}

bool ICPFitter::get_cloud_normals(const PointCloud::Ptr& cloud,
                                  PointCloudWithNormals::Ptr& cloud_normals) {
    pcl::NormalEstimation<Point, PointNormal> norm_est;
    pcl::search::KdTree<Point>::Ptr tree(new pcl::search::KdTree<Point>());
    norm_est.setViewPoint(0.0,0.0,3.0);
    norm_est.setSearchMethod(tree);
    //norm_est.setKSearch(5);
    norm_est.setRadiusSearch(0.006);
    norm_est.setInputCloud(cloud);
    norm_est.compute(*cloud_normals);
    ROS_DEBUG("cloud.size() + %lu", cloud->points.size());
    ROS_DEBUG("cloud_normals.size() = %lu", cloud_normals->points.size());
    return true;
}

void ICPFitter::set_target_cloud(const PointCloud::Ptr &target_cloud) {
    //icp_position_only_.setInputTarget(target_cloud);
    PointCloudWithNormals::Ptr target_cloud_normals(new PointCloudWithNormals());
    if(use_normals_) {

        if(!get_cloud_normals(target_cloud,
                              target_cloud_normals)) {
            ROS_ERROR("Couldn't get cloud normals");
            return;
        }
        std::vector<int> non_nan_idx;
        pcl::removeNaNNormalsFromPointCloud(*target_cloud_normals,
                                            *target_cloud_normals,
                                            non_nan_idx);
        ROS_DEBUG("non_nan_idx.size() = %lu", non_nan_idx.size());
        for(unsigned int i = 0; i < non_nan_idx.size(); i++) {
            target_cloud_normals->points[i].x = target_cloud->points[non_nan_idx[i]].x;
            target_cloud_normals->points[i].y = target_cloud->points[non_nan_idx[i]].y;
            target_cloud_normals->points[i].z = target_cloud->points[non_nan_idx[i]].z;
        }
    }

    pcl::DefaultPointRepresentation<pcl::PointNormal> validator;
    for(unsigned int i = 0; i < target_cloud_normals->points.size(); i++) {
        if(!validator.isValid(target_cloud_normals->points[i])) {
            ROS_ERROR("Got invalid point");
            ROS_ERROR("Invalid point: (%f, %f, %f), (%f,%f, %f)", target_cloud_normals->points[i].x,
                                                                  target_cloud_normals->points[i].y,
                                                                  target_cloud_normals->points[i].z,
                                                                  target_cloud_normals->points[i].normal_x,
                                                                  target_cloud_normals->points[i].normal_y,
                                                                  target_cloud_normals->points[i].normal_z);
        }
    }

    if(use_sparse_icp_) {
        ROS_ERROR("Not implemented");
        /*
        if(use_normals_) {
            sparse_icp_target_normals_cloud_ = target_cloud_normals;
        } else {
            sparse_icp_target_cloud_ = target_cloud;
        }*/

    } else {
        if(use_normals_) {
            icp_normal_.setInputTarget(target_cloud_normals);
        } else {
            icp_point_.setInputTarget(target_cloud);
        }
    }


}

bool ICPFitter::do_icp(const PointCloud::Ptr& source_cloud,
                       const std::vector<Matrix4>& perturbations,
                       std::vector<PointCloud::Ptr>& aligned_clouds,
                       std::vector<Matrix4>& transformations,
                       std::vector<double >& fitness_scores,
                       std::vector<bool>& converged) {
    aligned_clouds.clear();
    transformations.clear();
    fitness_scores.clear();
    converged.clear();

    if(perturbations.size() <= 0) {
        ROS_ERROR("No perturbations were provided");
        return false;
    }

    std::vector<PointCloud::Ptr> perturbed_clouds;
    for(unsigned int i = 0; i < perturbations.size(); i++) {
        PointCloud::Ptr perturbed_cloud(new PointCloud());
        pcl::transformPointCloud(*source_cloud,
                                 *perturbed_cloud,
                                 perturbations[i]);
        perturbed_clouds.push_back(perturbed_cloud);
    }

    bool one_converged = do_icp(perturbed_clouds,
                                aligned_clouds,
                                transformations,
                                fitness_scores,
                                converged);

    return one_converged;
}

bool ICPFitter::do_icp(const std::vector<PointCloud::Ptr> &source_clouds,
                       std::vector<PointCloud::Ptr> &aligned_clouds,
                       std::vector<Matrix4> &transformations,
                       std::vector<double> &fitness_scores,
                       std::vector<bool>& converged) {
    bool one_converged = false;
    aligned_clouds.clear();
    transformations.clear();
    fitness_scores.clear();
    converged.clear();
    for(unsigned int i = 0; i < source_clouds.size(); i++) {
        PointCloud::Ptr aligned_cloud(new PointCloud());
        Matrix4 transformation;
        double fitness_score;
        bool icp_converged = do_icp(source_clouds[i],
                                    aligned_cloud,
                                    transformation,
                                    fitness_score);
        aligned_clouds.push_back(aligned_cloud);
        transformations.push_back(transformation);
        fitness_scores.push_back(fitness_score);
        converged.push_back(icp_converged);

        if(icp_converged) {
            one_converged = true;
        }
    }

    return one_converged;
}

bool ICPFitter::do_icp(const PointCloud::Ptr &source_cloud,
                       PointCloud::Ptr &aligned_cloud,
                       Matrix4 &transformation,
                       double &fitness_score) {



    //PointCloud::Ptr position_only_aligned(new PointCloud());
    //icp_position_only_.setInputSource(source_cloud);
    //icp_position_only_.align(*position_only_aligned);

    bool converged = false;
    if(use_sparse_icp_) {
        ROS_ERROR("Not implemented");
        /*
        SICP::Parameters params;
        params.p = 1.0;
        params.print_icpn = true;
        Eigen::Matrix3Xd sparse_source(3, source_cloud->points.size());
        for(unsigned int i = 0; i < source_cloud->points.size(); i++) {
            sparse_source(0,i) = source_cloud->points[i].x;
            sparse_source(1,i) = source_cloud->points[i].y;
            sparse_source(2,i) = source_cloud->points[i].z;
        }
        if(use_normals_) {
            Eigen::Matrix3Xd sparse_target(3, sparse_icp_target_normals_cloud_->points.size());
            Eigen::Matrix3Xd sparse_target_normals(3, sparse_icp_target_normals_cloud_->points.size());
            for(unsigned int i = 0; i < sparse_icp_target_normals_cloud_->points.size(); i++) {
                sparse_target(0,i) = sparse_icp_target_normals_cloud_->points[i].x;
                sparse_target(1,i) = sparse_icp_target_normals_cloud_->points[i].y;
                sparse_target(2,i) = sparse_icp_target_normals_cloud_->points[i].z;

                sparse_target_normals(0,i) = sparse_icp_target_normals_cloud_->points[i].normal_x;
                sparse_target_normals(1,i) = sparse_icp_target_normals_cloud_->points[i].normal_y;
                sparse_target_normals(2,i) = sparse_icp_target_normals_cloud_->points[i].normal_z;
            }
            SICP::point_to_plane(sparse_source,
                                 sparse_target,
                                 sparse_target_normals,
                                 params);

        } else {
            Eigen::Matrix3Xd sparse_target(3, sparse_icp_target_cloud_->points.size());
            for(unsigned int i = 0; i < sparse_icp_target_cloud_->points.size(); i++) {
                sparse_target(0,i) = sparse_icp_target_cloud_->points[i].x;
                sparse_target(1,i) = sparse_icp_target_cloud_->points[i].y;
                sparse_target(2,i) = sparse_icp_target_cloud_->points[i].z;
            }
            SICP::point_to_point(sparse_source,
                                 sparse_target,
                                 params);
        }
        aligned_cloud->points.resize(source_cloud->points.size());
        for(unsigned int i = 0; i < source_cloud->points.size(); i++) {
            aligned_cloud->points[i].x = sparse_source(0,i);
            aligned_cloud->points[i].y = sparse_source(1,i);
            aligned_cloud->points[i].z = sparse_source(2,i);
        }
        aligned_cloud->header.frame_id = source_cloud->header.frame_id;
        aligned_cloud->header.stamp = source_cloud->header.stamp;
        aligned_cloud->is_dense = source_cloud->is_dense;
        aligned_cloud->height = 1;
        aligned_cloud->width = aligned_cloud->points.size();
        tesvd_->estimateRigidTransformation(*source_cloud, *aligned_cloud, transformation);
        converged = true;*/
    } else {
        if(use_normals_) {
            PointCloudWithNormals::Ptr source_cloud_normals(new PointCloudWithNormals());
            ROS_DEBUG("Getting normals...");
            get_cloud_normals(source_cloud,
                              source_cloud_normals);
            std::vector<int> non_nan_idx;
            pcl::removeNaNNormalsFromPointCloud(*source_cloud_normals,
                                                *source_cloud_normals,
                                                non_nan_idx);
            ROS_DEBUG("non_idx.size() = %lu", non_nan_idx.size());
            ROS_DEBUG("Copying point cloud...");
            for(unsigned int i = 0; i < non_nan_idx.size(); i++) {
                source_cloud_normals->points[i].x = source_cloud->points[non_nan_idx[i]].x;
                source_cloud_normals->points[i].y = source_cloud->points[non_nan_idx[i]].y;
                source_cloud_normals->points[i].z = source_cloud->points[non_nan_idx[i]].z;
            }

            pcl::DefaultPointRepresentation<pcl::PointNormal> validator;
            for(unsigned int i = 0; i < source_cloud_normals->points.size(); i++) {
                if(!validator.isValid(source_cloud_normals->points[i])) {
                    ROS_ERROR("Got invalid point");
                    ROS_ERROR("Invalid point: (%f, %f, %f), (%f,%f, %f)", source_cloud_normals->points[i].x,
                                                                          source_cloud_normals->points[i].y,
                                                                          source_cloud_normals->points[i].z,
                                                                          source_cloud_normals->points[i].normal_x,
                                                                          source_cloud_normals->points[i].normal_y,
                                                                          source_cloud_normals->points[i].normal_z);
                }
            }
            if(use_covar_sampling_) {
                PointCloudWithNormals::Ptr covar_source_cloud_normals(new PointCloudWithNormals());
                covar_sample(source_cloud_normals,
                             source_cloud_normals->points.size()/4,
                             covar_source_cloud_normals);
                source_cloud_normals = covar_source_cloud_normals;
            }
            ROS_DEBUG("Setting input source...");
            icp_normal_.setInputSource(source_cloud_normals);

            PointCloudWithNormals::Ptr source_cloud_normals_aligned(new PointCloudWithNormals());
            ROS_DEBUG("Aligning point cloud...");
            icp_normal_.align(*source_cloud_normals_aligned);
            ROS_DEBUG("Copying point cloud...");
            pcl::copyPointCloud(*source_cloud_normals_aligned, *aligned_cloud);
            fitness_score = icp_normal_.getFitnessScore(icp_normal_.getMaxCorrespondenceDistance());
            ROS_DEBUG("Estimating rigid transformation...");
            tenorm_->estimateRigidTransformation(*source_cloud_normals, *source_cloud_normals_aligned, transformation);
            converged = icp_normal_.hasConverged();
            ROS_DEBUG("...icp complete");

        } else {
            if(use_covar_sampling_) {
                ROS_WARN("Must use normals in order to do covariance sampling, continuing without sampling");
            }
            icp_point_.setInputSource(source_cloud);
            icp_point_.align(*aligned_cloud);
            fitness_score = icp_point_.getFitnessScore(icp_point_.getMaxCorrespondenceDistance());
            tesvd_->estimateRigidTransformation(*source_cloud, *aligned_cloud, transformation);
            converged = icp_point_.hasConverged();
        }
    }


    return converged;

    /*
    icp_position_only_.setInputSource(source_cloud);
    icp_position_only_.align(*aligned_cloud);
    tepo_->estimateRigidTransformation(*source_cloud, *aligned_cloud, transformation);
    fitness_score = icp_position_only_.getFitnessScore(icp_position_only_.getMaxCorrespondenceDistance());
    return icp_position_only_.hasConverged();
    */

}

bool ICPFitter::get_average_cloud_density(const PointCloud::Ptr &cloud,
                                          double& avg_distance) {
    avg_distance = 0.0;
    PointCloud::Ptr xy_cloud(new PointCloud(*cloud));
    for(unsigned int i = 0; i < xy_cloud->points.size(); i++) {
        xy_cloud->points[i].z = 0.0;
    }
    pcl::KdTreeFLANN<Point> tree;
    tree.setInputCloud(xy_cloud);
    std::vector<int> nn_idx(2);
    std::vector<float> nn_square_distance(2);
    int nn_comparisons = 0;
    for(unsigned int i= 0; i < xy_cloud->points.size(); i++) {
        if(tree.nearestKSearch(xy_cloud->points[i], 2, nn_idx, nn_square_distance) > 0) {
            avg_distance += std::sqrt(nn_square_distance[1]);
            nn_comparisons++;
        }
    }

    if(nn_comparisons <= 0) {
        ROS_ERROR("No nearest neighbors were found");
        return false;
    }

    avg_distance = avg_distance / nn_comparisons;

    return true;

}

bool ICPFitter::gp_interpolate_cloud(const PointCloud::Ptr &cloud,
                                     double grid_size,
                                     PointCloud::Ptr &interpolated_cloud) {
    CovFuncND func(2, 1.0,1.0);
    gaussian_process::SingleGP gp(func, 5.0, true);
    TVector<TDoubleVector> gp_data(cloud->points.size());
    TVector<double> gp_labels(cloud->points.size());

    for(unsigned int i = 0; i < cloud->points.size(); i++) {
        gp_data[i].resize(2);
        gp_data[i][0] = cloud->points[i].x;
        gp_data[i][1] = cloud->points[i].y;

        gp_labels[i] = cloud->points[i].z;
    }
    gp.SetData(gp_data, gp_labels);
    gp.OptimizeGP();

    // Determine x and y ranges
    double min_x = std::numeric_limits<float>::max();
    double max_x = -1*std::numeric_limits<float>::max();
    double min_y = std::numeric_limits<float>::max();
    double max_y = -1*std::numeric_limits<float>::max();
    for(unsigned int i = 0; i < cloud->points.size(); i++) {
        if(cloud->points[i].x < min_x) {
            min_x = cloud->points[i].x;
        }
        if(cloud->points[i].x > max_x) {
            max_x = cloud->points[i].x;
        }

        if(cloud->points[i].y < min_y) {
            min_y = cloud->points[i].y;
        }
        if(cloud->points[i].y > max_y) {
            max_y = cloud->points[i].y;
        }
    }

    double cur_x = min_x + grid_size/2.0;
    while(cur_x < max_x) {
        double cur_y = min_y + grid_size/2.0;
        while(cur_y < max_y) {
            double inter_val, inter_var;
            TVector<double> search_point(2);
            search_point[0] = cur_x;
            search_point[1] = cur_y;
            gp.Evaluate(search_point, inter_val, inter_var);
            interpolated_cloud->points.push_back(Point(cur_x, cur_y, inter_val));
            cur_y += grid_size;
        }
        cur_x += grid_size;
    }

    interpolated_cloud->header.frame_id = cloud->header.frame_id;
    interpolated_cloud->header.stamp = cloud->header.stamp;
    interpolated_cloud->is_dense = cloud->is_dense;
    interpolated_cloud->height = 1;
    interpolated_cloud->width = interpolated_cloud->points.size();

    return true;
}

bool ICPFitter::compute_interp_val(const PointCloud::Ptr &cloud,
                                   const std::vector<int> &nn_idx,
                                   const std::vector<double> &distances,
                                   const COORDINATE_USE &cu,
                                   double &interp_val) {
    interp_val = 0.0;
    int K = nn_idx.size();
    std::vector<double> weights(K);
    if(K == 1) {
        weights[0] = 1.0;
    } else {
        double distances_sum = 0.0;
        for(unsigned int i = 0; i < K; i++) {
            distances_sum += distances[i];
            for(unsigned int j = 0; j < K; j++) {
                if(i != j) {
                    weights[i] += distances[j];
                }
            }
        }
        for(unsigned int i = 0; i < K; i++) {
            weights[i] = weights[i]/(distances_sum*(K-1));
        }
    }

    for(unsigned int i = 0 ; i < K; i++) {
        switch(cu) {
        case USE_XY:
            interp_val += weights[i]*cloud->points[nn_idx[i]].z;
            break;
        case USE_XZ:
            interp_val += weights[i]*cloud->points[nn_idx[i]].y;
            break;
        case USE_YZ:
            interp_val += weights[i]*cloud->points[nn_idx[i]].x;
            break;
        default:
            ROS_ERROR("Shouldn't get here ( bilinear interpolate, interval compute");
            return false;
        }

    }
    return true;
}

bool ICPFitter::bilinear_interpolate_cloud(const PointCloud::Ptr &cloud,
                                           bool use_x,
                                           bool use_y,
                                           bool use_z,
                                           double grid_size,
                                           float search_radius,
                                           PointCloud::Ptr &interpolated_cloud) {
    assert(!(use_x&&use_y&&use_z));
    assert((use_x&&use_y) ||
           (use_x&&use_z) ||
           (use_y&&use_z));

    interpolated_cloud->clear();

    Point min_pt;
    Point max_pt;
    pcl::getMinMax3D(*cloud, min_pt, max_pt);

    COORDINATE_USE cu;
    if(use_x&&use_y) {
        cu = USE_XY;
    } else if(use_x&&use_z) {
        cu = USE_XZ;
    } else {
        cu = USE_YZ;
    }

    double dim1_min, dim1_max;
    double dim2_min, dim2_max;
    pcl::PointCloud<pcl::PointXY>::Ptr two_dim_cloud(new pcl::PointCloud<pcl::PointXY>());
    pcl::PointXY two_dim_point;
    for(unsigned int i = 0; i < cloud->points.size(); i++) {
        switch(cu) {
        case USE_XY:
            two_dim_point.x = cloud->points[i].x;
            two_dim_point.y = cloud->points[i].y;
            dim1_min = min_pt.x;
            dim2_min = min_pt.y;
            dim1_max = max_pt.x;
            dim2_max = max_pt.y;
            break;
        case USE_XZ:
            two_dim_point.x = cloud->points[i].x;
            two_dim_point.y = cloud->points[i].z;
            dim1_min = min_pt.x;
            dim2_min = min_pt.z;
            dim1_max = max_pt.x;
            dim2_max = max_pt.z;
            break;
        case USE_YZ:
            two_dim_point.x = cloud->points[i].y;
            two_dim_point.y = cloud->points[i].z;
            dim1_min = min_pt.y;
            dim2_min = min_pt.z;
            dim1_max = max_pt.y;
            dim2_max = max_pt.z;
            break;
        default:
            ROS_ERROR("Shouldn't get here (default case of bilinear interpolate");
            return false;
        }
        two_dim_cloud->points.push_back(two_dim_point);
    }

    pcl::KdTreeFLANN<pcl::PointXY> tree;
    tree.setInputCloud(two_dim_cloud);

    std::vector<int> nn_idx;
    std::vector<float> nn_square_distance;

    double dim1_cur = dim1_min + grid_size/2.0;

    while(dim1_cur < dim1_max) {
        double dim2_cur = dim2_min + grid_size/2.0;
        while(dim2_cur < dim2_max) {
            nn_idx.clear();
            nn_square_distance.clear();
            pcl::PointXY search_point;
            search_point.x = dim1_cur;
            search_point.y = dim2_cur;
            if(tree.radiusSearch(search_point, search_radius, nn_idx, nn_square_distance) > 0) {
                Point interp_point;
                while(ros::ok()) {
                    int K = nn_idx.size();

                    std::vector<double> distances(K);
                    for(unsigned int i = 0; i < K; i++) {
                        distances[i] = std::sqrt(nn_square_distance[i]);
                    }

                    double interp_val = 0.0;
                    compute_interp_val(cloud,
                                       nn_idx,
                                       distances,
                                       cu,
                                       interp_val);

                    switch(cu) {
                    case USE_XY:
                        interp_point.x = dim1_cur;
                        interp_point.y = dim2_cur;
                        interp_point.z = interp_val;
                        break;
                    case USE_XZ:
                        interp_point.x = dim1_cur;
                        interp_point.y = interp_val;
                        interp_point.z = dim2_cur;
                        break;
                    case USE_YZ:
                        interp_point.x = interp_val;
                        interp_point.y = dim1_cur;
                        interp_point.z = dim2_cur;
                        break;
                    default:
                        ROS_ERROR("Shouldn't get here ( bilinear interpolate, interval init");
                    }

                    bool all_in_sphere = true;
                    std::vector<int> upper_nn_indices;
                    for(unsigned int i = 0; i < nn_idx.size(); i++) {
                        double square_dist = std::pow(interp_point.x-cloud->points[nn_idx[i]].x,2)+
                                             std::pow(interp_point.y-cloud->points[nn_idx[i]].y,2)+
                                             std::pow(interp_point.z-cloud->points[nn_idx[i]].z,2);
                        if(square_dist > search_radius*search_radius) {
                            all_in_sphere = false;
                        }
                        if(cu == USE_XY && interp_val <= cloud->points[nn_idx[i]].z) {
                            upper_nn_indices.push_back(i);
                        } else if(cu == USE_XZ && interp_val <= cloud->points[nn_idx[i]].y) {
                            upper_nn_indices.push_back(i);
                        } else if(cu == USE_YZ && interp_val <= cloud->points[nn_idx[i]].x) {
                            upper_nn_indices.push_back(i);
                        }
                    }

                    if(all_in_sphere) {
                        break;
                    }
                    assert(upper_nn_indices.size() > 0);
                    std::vector<int> new_nn_idx;
                    std::vector<float> new_nn_square_distance;
                    for(unsigned int i = 0; i < upper_nn_indices.size(); i++) {
                        new_nn_idx.push_back(nn_idx[upper_nn_indices[i]]);
                        new_nn_square_distance.push_back(nn_square_distance[upper_nn_indices[i]]);
                    }
                    nn_idx = new_nn_idx;
                    nn_square_distance = new_nn_square_distance;
                }

                interpolated_cloud->points.push_back(interp_point);

            }
            dim2_cur += grid_size;
        }
        dim1_cur += grid_size;
    }

    interpolated_cloud->header.frame_id = cloud->header.frame_id;
    interpolated_cloud->header.stamp = cloud->header.stamp;
    interpolated_cloud->is_dense = cloud->is_dense;
    interpolated_cloud->height = 1;
    interpolated_cloud->width = interpolated_cloud->points.size();

    return true;

}
/*
bool ICPFitter::bilinear_interpolate_cloud(const PointCloud::Ptr& cloud,
                                           double grid_size,
                                           float search_radius,
                                           PointCloud::Ptr& interpolated_cloud) {

    // Determine x and y ranges
    double min_x = std::numeric_limits<float>::max();
    double max_x = -1*std::numeric_limits<float>::max();
    double min_y = std::numeric_limits<float>::max();
    double max_y = -1*std::numeric_limits<float>::max();
    for(unsigned int i = 0; i < cloud->points.size(); i++) {
        if(cloud->points[i].x < min_x) {
            min_x = cloud->points[i].x;
        }
        if(cloud->points[i].x > max_x) {
            max_x = cloud->points[i].x;
        }

        if(cloud->points[i].y < min_y) {
            min_y = cloud->points[i].y;
        }
        if(cloud->points[i].y > max_y) {
            max_y = cloud->points[i].y;
        }
    }

    // Build nn grid cloud and tree
    PointCloud::Ptr xy_cloud(new PointCloud(*cloud));
    for(unsigned int i = 0; i < xy_cloud->points.size(); i++) {
        xy_cloud->points[i].z = 0.0;
    }

    pcl::KdTreeFLANN<Point> tree;
    tree.setInputCloud(xy_cloud);

    std::vector<int> nn_idx;
    std::vector<float> nn_square_distance;

    double cur_x = min_x + grid_size/2.0;
    while(cur_x < max_x) {
        double cur_y = min_y + grid_size/2.0;
        while(cur_y < max_y) {
            nn_idx.clear();
            nn_square_distance.clear();
            Point search_point(cur_x, cur_y,0.0);

            if(tree.radiusSearch(search_point, search_radius, nn_idx, nn_square_distance) > 0) {
                int K = nn_idx.size();

                std::vector<double> distances(K);
                for(unsigned int i = 0; i < K; i++) {
                    distances[i] = std::sqrt(nn_square_distance[i]);
                }

                std::vector<double> weights(K);
                if(K == 1) {
                    weights[0] = 1.0;
                } else {
                    double distances_sum = 0.0;
                    for(unsigned int i = 0; i < K; i++) {
                        distances_sum += distances[i];
                        for(unsigned int j = 0; j < K; j++) {
                            if(i != j) {
                                weights[i] += distances[j];
                            }
                        }
                    }
                    for(unsigned int i = 0; i < K; i++) {
                        weights[i] = weights[i]/(distances_sum*(K-1));
                    }
                }
                double inter_val = 0.0;
                for(unsigned int i = 0 ; i < K; i++) {
                    inter_val += weights[i]*cloud->points[nn_idx[i]].z;
                }
                interpolated_cloud->points.push_back(Point(cur_x, cur_y, inter_val));
            }

            cur_y += grid_size;
        }
        cur_x += grid_size;
    }

    interpolated_cloud->header.frame_id = cloud->header.frame_id;
    interpolated_cloud->header.stamp = cloud->header.stamp;
    interpolated_cloud->is_dense = cloud->is_dense;
    interpolated_cloud->height = 1;
    interpolated_cloud->width = interpolated_cloud->points.size();

    return true;
}
*/
bool ICPFitter::get_use_reciprocal_correspondences() {
    if(use_normals_) {
        return icp_point_.getUseReciprocalCorrespondences();
    } else {
        return icp_point_.getUseReciprocalCorrespondences();
    }

}

double ICPFitter::get_max_inlier_distance() {
    if(use_normals_) {
        return icp_normal_.getRANSACOutlierRejectionThreshold();
    } else {
        return icp_point_.getRANSACOutlierRejectionThreshold();
    }

}

double ICPFitter::get_max_correspondence_distance() {
    if(use_normals_) {
        return icp_normal_.getMaxCorrespondenceDistance();
    } else {
        return icp_point_.getMaxCorrespondenceDistance();
    }


}

int ICPFitter::get_max_iterations() {
    if(use_normals_) {
        return icp_normal_.getMaximumIterations();
    } else {
        return icp_point_.getMaximumIterations();
    }

}

double ICPFitter::get_ransac_iterations() {
    if(use_normals_) {
        return icp_normal_.getRANSACIterations();
    } else {
        return icp_point_.getRANSACIterations();
    }

}

double ICPFitter::get_transform_eps_converge_distance() {
    if(use_normals_) {
        return icp_normal_.getTransformationEpsilon();
    } else {
        return icp_point_.getTransformationEpsilon();
    }

}

double ICPFitter::get_fitness_eps_converge_distance() {
    if(use_normals_) {
        return icp_normal_.getEuclideanFitnessEpsilon();
    } else {
        return icp_point_.getEuclideanFitnessEpsilon();
    }

}

void pointcloud_to_rgbpointcloud(const pcl::PointCloud<pcl::PointXYZ>::Ptr& cloud,
                                 const pcl::PointXYZRGB& color,
                                 pcl::PointCloud<pcl::PointXYZRGB>::Ptr& color_cloud) {

    color_cloud->header = cloud->header;
    color_cloud->is_dense = cloud->is_dense;
    color_cloud->height = cloud->height;
    color_cloud->width = cloud->width;
    pcl::PointXYZRGB rgb_point;
    rgb_point.r = color.r;
    rgb_point.g = color.g;
    rgb_point.b = color.b;
    for(unsigned int i = 0; i < cloud->points.size(); i++) {
        rgb_point.x = cloud->points[i].x;
        rgb_point.y = cloud->points[i].y;
        rgb_point.z = cloud->points[i].z;
        color_cloud->points.push_back(rgb_point);
    }
}

void pointclouds_to_rgbpointclouds(const std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr>& clouds,
                                   const std::vector<pcl::PointXYZRGB>& colors,
                                   std::vector<pcl::PointCloud<pcl::PointXYZRGB>::Ptr>& color_clouds) {
    color_clouds.clear();
    for(unsigned int i = 0; i < clouds.size(); i++) {
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr tmp_ptr(new pcl::PointCloud<pcl::PointXYZRGB>());
        color_clouds.push_back(tmp_ptr);
        pointcloud_to_rgbpointcloud(clouds[i],
                                    colors[i],
                                    color_clouds[i]);
    }
}

int main(int argc, char** argv) {
    // Test icp class
    ros::init(argc, argv, "icp_fitter");
    ros::NodeHandle node;

    pcl::PointCloud<pcl::PointXYZ>::Ptr orig_cloud(new pcl::PointCloud<pcl::PointXYZ>());
    orig_cloud->width = 100;
    orig_cloud->height = 1;
    orig_cloud->is_dense = false;
    orig_cloud->points.resize(orig_cloud->width*orig_cloud->height);
    for(size_t i = 0; i < 10; i++) {
        for(size_t j = 0; j < 10; j++) {
            orig_cloud->points[10*i+j].x = i;
            orig_cloud->points[10*i+j].y = j;
            orig_cloud->points[10*i+j].z = 0.0;
        }

    }
    ICPFitter icpf;
    icpf.print_settings();
    icpf.set_target_cloud(orig_cloud);


    pcl::PointCloud<pcl::PointXYZ>::Ptr shifted_cloud(new pcl::PointCloud<pcl::PointXYZ>());
    *shifted_cloud = *orig_cloud;
    for(size_t i = 0; i < shifted_cloud->points.size(); i++) {
        shifted_cloud->points[i].x += 0.25*((rand() / (RAND_MAX+1.0f))-0.5);
        shifted_cloud->points[i].y += 0.25*((rand() / (RAND_MAX+1.0f))-0.5);
        shifted_cloud->points[i].z += 0.25*((rand() / (RAND_MAX+1.0f))-0.5);
    }

    tf::Transform neg_x_tf(tf::Quaternion(0,0,0,1), tf::Vector3(-0.1,0.0,0.0));
    tf::Transform pos_x_tf(tf::Quaternion(0,0,0,1), tf::Vector3(0.1 ,0.0,0.0));
    tf::Transform neg_y_tf(tf::Quaternion(0,0,0,1), tf::Vector3(0.0,-0.1,0.0));
    tf::Transform pos_y_tf(tf::Quaternion(0,0,0,1), tf::Vector3(0.0, 0.1,0.0));


    std::vector<ICPFitter::Matrix4> perturbation_list(4);
    Eigen::Affine3d tmp;

    tf::transformTFToEigen(neg_x_tf, tmp);
    perturbation_list[0] = tmp.matrix().cast<float>();

    tf::transformTFToEigen(pos_x_tf, tmp);
    perturbation_list[1] = tmp.matrix().cast<float>();

    tf::transformTFToEigen(neg_y_tf, tmp);
    perturbation_list[2] = tmp.matrix().cast<float>();

    tf::transformTFToEigen(pos_y_tf, tmp);
    perturbation_list[3] = tmp.matrix().cast<float>();

    std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr> aligned_clouds;
    std::vector<ICPFitter::Matrix4> transformations;
    std::vector<double> fitness_scores;
    std::vector<bool> converged;

    ROS_INFO("Going to do icp");
    icpf.do_icp(shifted_cloud,
                perturbation_list,
                aligned_clouds,
                transformations,
                fitness_scores,
                converged);

    std::vector<pcl::PointCloud<pcl::PointXYZRGB>::Ptr> aligned_color_clouds;
    pcl::PointXYZRGB red; red.r = 255; red.g = 0; red.b = 0;
    pcl::PointXYZRGB green; green.r = 0; green.g = 255; green.b = 0;
    pcl::PointXYZRGB blue; blue.r = 0; blue.g = 0; blue.b = 255;
    pcl::PointXYZRGB purple; purple.r = 255; purple.g = 0; purple.b = 255;
    std::vector<pcl::PointXYZRGB> colors;
    colors.push_back(red); colors.push_back(green); colors.push_back(blue); colors.push_back(purple);
    ROS_INFO("Going to get color clouds");
    pointclouds_to_rgbpointclouds(aligned_clouds,
                                  colors,
                                  aligned_color_clouds);

    ROS_INFO("Going to view clouds");
    pcl::visualization::CloudViewer viewer("Simple Cloud Viewer");
    viewer.showCloud(orig_cloud, "orig_cloud");
    viewer.showCloud(aligned_color_clouds[0], "shifted_cloud0");
    viewer.showCloud(aligned_color_clouds[1], "shifted_cloud1");
    viewer.showCloud(aligned_color_clouds[2], "shifted_cloud2");
    viewer.showCloud(aligned_color_clouds[3], "shifted_cloud3");
    while(!viewer.wasStopped()) {

    }
}
